/**
 * Função para formatar um número colocando pontos.
 * @author Víctor Vaz <victor-vaz@hotmail.com>
 * @param n Número para ser formatado.
 * @returns {string} Número formatado.
 */
function formatarNumero(n, prefix = "", fractalDigits = 2) {
    if (!n) return "-"
    return prefix + " " + n.toLocaleString('pt-br', {minimumFractionDigits: fractalDigits});
}


export default formatarNumero