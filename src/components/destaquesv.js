import React, { useEffect, useState } from 'react'
import { Col, Navbar, Row, Container, Table, Button, Form, OverlayTrigger, Tooltip, Carousel, ButtonGroup, Accordion } from 'react-bootstrap'
import styled from 'styled-components'
import { getRealTimeRadar } from '../services/trader'
import { ResponsiveScatterPlot } from '@nivo/scatterplot'
import { acao } from '../util/objeto'
import history from '../config/history';
import Layout from './layout/layout'
// import DoubleRangeSlider from '../util/doubleslider/doubleSlider'
// import arrdata from '../util/datatemp'

//Ler https://www.npmjs.com/package/react-chartjs-2

/** Não carrega chaves que são texto */
export const loadKeys = (objeto, chaves) => {


    for (const [key, value] of Object.entries(objeto)) {
        if (!isNaN(parseInt(value))) {
            if (!chaves.includes(key)) {
                chaves.push(key)
            }
        }
    }

} 

/**Lê radarRealtime: `cache/radarRealtime.csv` */
const Destaquesv = () => {

    const [setores, setSetores] = useState([])

    /**Todos os papéis  */
    const [allPapers, setAllPapers] = useState([{}])

    /**Chaves serão usadas como eixos selecionados */
    const [chaves, setChaves] = useState([])

    /**Este é o objeto que será usado pelo gráfico */
    const [chartData, setChartData] = useState([
        {
            id: "LOADING",
            data: [{ x: 0, y: 0 }]
        }
    ]
    )
    const [toChart, setToChart] = useState({
        Xaxis: "",
        Yaxis: "",
        Classificar: "pontosSoma",
        dcrescente: 1,
        chartData: []
    })

    const toggleDCrescente = () => {
        setToChart(
            {
                ...toChart,
                dcrescente: toChart.dcrescente * -1
            }
        )
    }

    const [apenasOpts, setapenasOpts] = useState(false)
    const arrlistaTikersOpcoes = ["KLBN11", "SMLS3", "BOVA11", "VVAR3", "PETR4", "MGLU3", "VALE3", "COGN3", "IRBR3", "ABEV3", "ENBR3",
        "WEGE3", "ELET3", "EQTL3", "QUAL3", "B3SA3", "BBDC4", "GGBR4", "RAIL3", "BBSE3", "GOAU4", "RENT3",
        "BRAP4", "HYPE3", "SANB11", "BRDT3", "ITSA4", "BRFS3", "ITUB4", "SUZB3", "BRKM5", "JBSS3", "TAEE11",
        "BRML3", "LAME4", "TIMS3", "CCRO3", "LREN3", "UGPA3", "CMIG4", "USIM5", "MRFG3", "EMBR3", "CSNA3",
        "MRVE3", "VIVT3", "ECOR3", "MULT3", "BBAS3"]

    const ListX = () => {
        return (
            <Form.Group >
                <Form.Control as="select" custom name="Xaxis" onChange={handleChange} value={toChart.Xaxis}>
                    {/* <option value="">-----</option> */}
                    {chaves.map((chave, i) => (
                        <option key={i} value={chaves[i]}> {chave} </option>
                    ))}
                </Form.Control>
            </Form.Group>
        )
    }

    const ListY = () => {
        return (
            <Form.Group >
                <Form.Control as="select" custom name="Yaxis" onChange={handleChange} value={toChart.Yaxis}>
                    {/* <option value="">-----</option> */}
                    {chaves.map((chave, i) => (
                        <option key={i} value={chaves[i]}> {chave} </option>
                    ))}
                </Form.Control>
            </Form.Group>
        )
    }

    const Classificar = () => {
        return (
            <Form.Group >
                <Form.Control as="select" custom name="Classificar" onChange={handleChange} value={toChart.Classificar}>
                    {/* <option value="">-----</option> */}
                    {chaves.map((chave, i) => (
                        <option key={i} value={chaves[i]}> {chave} </option>
                    ))}
                </Form.Control>
            </Form.Group>
        )
    }





    const carregarRealTime = () => {

        const getdataOne = getRealTimeRadar()
            .then((res) => {

                let setores = []
                
                res.data.map(item => {
                    if (!setores.includes(item.setor)) {
                        setores.push(item.setor)
                    }
                })
                setSetores(setores)

                res.data = res.data.sort(function (a, b) {
                    if (a[toChart.Classificar] > b[toChart.Classificar]) {
                        return -1 * toChart.dcrescente;
                    }
                    if (a[toChart.Classificar] < b[toChart.Classificar]) {
                        return 1 * toChart.dcrescente;
                    }
                    return 0;
                })

                let dadosRes = res.data
                setAllPapers(dadosRes)
                debugger
                let forArrData = dadosRes.map(item => makeArrdata(item.TICKER, item[toChart.Xaxis], item[toChart.Yaxis], item.setor))

                setChartData(forArrData)

                loadKeys(dadosRes[0], chaves)
                RefilterRealTime()

            })
    }


    /**As mudanças no state toChart disparam o useefect que chama este refilter.
     * Refilter reaplica todos os filters que estão definidos também no toChart
     * toChart recebe tanto os filtros quanto os dados (em chartData) filtrados na sub abaixo. 
     */
    const RefilterRealTime = () => {
        // console.log("toChart.limite", toChart)
        let forArrData = allPapers.sort(function (a, b) {
            if (a[toChart.Classificar] > b[toChart.Classificar]) {

                return 1 * toChart.dcrescente;
            }
            if (a[toChart.Classificar] < b[toChart.Classificar]) {
                return -1 * toChart.dcrescente;
            }
            return 0;
        })


        forArrData = forArrData.map(item => makeArrdata(item.TICKER, item[toChart.Xaxis], item[toChart.Yaxis], item.setor))
        forArrData = forArrData.slice(0, toChart.limite)

        if (apenasOpts) {
            forArrData = forArrData.filter(item => arrlistaTikersOpcoes.includes(item.id))
        }


        if (toChart.setor) {
            forArrData = forArrData.filter(item => item.setor == toChart.setor)
        }

        
        if (toChart.papeis) {
            forArrData = forArrData.filter(item => toChart.papeis.includes(item.id))
        }

        setToChart(
            {
                ...toChart,
                chartData: forArrData
            }
        )
    }




    /** cria o objeto ´no formato que pode ser lido pelo componente do gráfico */
    function makeArrdata(TICKER, xvlue, yvalue, parsetor) {
        return {

            id: TICKER,
            data: [{ x: xvlue, y: yvalue }],
            setor: parsetor
        }
    }


    useEffect(
        () => {

            RefilterRealTime()
        }, [toChart.Xaxis, toChart.Yaxis, toChart.limite, toChart.Classificar, toChart.dcrescente, apenasOpts, toChart.setor])


    useEffect(() => {
        setInterval(() => {
            //ponha aqui a função que irá atualizar seu state
            // carregarRealTime()
        }, 15 * 60 * 1000)
    }, []);

    useEffect(() => {

        carregarRealTime()

    }, []);



    const MyResponsiveScatterPlot = ({ data, xlabel, ylabel }) => (
        <ResponsiveScatterPlot
            data={data}
            margin={{ top: 60, right: 140, bottom: 70, left: 90 }}
            xScale={{ type: 'linear', min: 0, max: 'auto' }}
            xFormat={function (e) { return "\n " + xlabel + ": " + e + "\n " }}
            yScale={{ type: 'linear', min: 0, max: 'auto' }}
            yFormat={function (e) { return "\n " + ylabel + ": " + e }}
            blendMode="multiply"
            axisTop={null}
            axisRight={null}
            axisBottom={{
                orient: 'bottom',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: xlabel,
                legendPosition: 'middle',
                legendOffset: 46
            }}
            axisLeft={{
                orient: 'left',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: ylabel,
                legendPosition: 'middle',
                legendOffset: -60
            }}
            legends={[
                {
                    anchor: 'bottom-right',
                    direction: 'column',
                    justify: false,
                    translateX: 130,
                    translateY: 0,
                    itemWidth: 100,
                    itemHeight: 12,
                    itemsSpacing: 5,
                    itemDirection: 'left-to-right',
                    symbolSize: 12,
                    symbolShape: 'circle',
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemOpacity: 1
                            }
                        }
                    ]
                }
            ]}
        />
    )

    const handleChange = (attr) => {
  
        let { value, name } = attr.target

        if (name === "theange") {
            console.log(value) // = value.toUpperCase() 
            //return
        }

        if (name === "papeis") {
            value = value.toUpperCase()
        }

        setToChart(
            {
                ...toChart,
                [name]: value//.toUpperCase()
            }
        )

    }

    const Setores = () => {
        return (
            <Form.Group >
                <Form.Control as="select" custom name="setor" onChange={handleChange} value={toChart.setor}>
                    <option value={""}> Sem filtro de Setor</option>
                    {setores.map((chave, i) => (
                        <option key={i} value={chave}> {chave} </option>
                    ))}
                </Form.Control>
            </Form.Group>
        )
    }


    const setAxis = (x, y) => {

        setToChart(
            {
                ...toChart,
                Xaxis: x, // 
                Yaxis: y //
            }
        )

    }


    const revertAxis = () => {

        let [i, j] = [toChart.Yaxis, toChart.Xaxis]
        setToChart(
            {
                ...toChart,
                Xaxis: i,//"pontosTecnicos",
                Yaxis: j //"pontosFundamentais"
            }
        )

    }


    const Acordeao = (props) => {
        return (
            <Accordion>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                    Filtros
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                    {props.children}
                </Accordion.Collapse>
            </Accordion>
        )
    }

    return (
        <Layout header={"Gráficos de ações."} footer={"Gráficos de ações."}>
            <Container fluid>

                <StDiv2>
                    <p> Data do Report : {allPapers[0].dataPesquisa}</p>
                    <Row>
                    <Button onClick={() => setAxis("lpaSobrePvp", "rsiMedia")} variant="outline-info">RSI Média x lpa/pvp</Button>
                        <Button onClick={() => setAxis("pontosTecnicos", "pontosFundamentais")} variant="outline-info">Tec x Fundamentos</Button>
                        <Button onClick={() => setAxis("rsi_storch_fast", "rsiToNode")} variant="outline-info">RSI FAST x LONG</Button>
                        <Button onClick={() => setAxis("deltaPreco", "deltaVol")} variant="outline-info">Dvol Dpreço</Button>
                        <Button onClick={() => setAxis("rsiMedia", "pontosFundamentais")} variant="outline-info"> RSI Media x Fundamentals </Button>
                        <Button onClick={() => setAxis("deltaPreco", "rsiMedia")} variant="outline-info">deltaPreco x rsiMedia</Button>
                        <Button onClick={() => setAxis("lpa", "precoPorLucro")} variant="outline-info">lpa x p/l</Button>
                        <Button onClick={() => setAxis("deltaPreco", "pontosFundamentais")} variant="outline-info">D$ x Fundtos</Button>
                        <Button onClick={() => setAxis("qualSaudeFund", "qualPrecoFund")} variant="outline-info"> Preco Saude Fund</Button>
                        <Button onClick={() => setapenasOpts(!apenasOpts)} variant={apenasOpts ? "dark" : "outline-info"}>{apenasOpts ? "Todas" : "Com Opções"}</Button>
                        <Button onClick={carregarRealTime} variant="outline-danger">Reload</Button>
                        <Button onClick={toggleDCrescente} variant={toChart.dcrescente === 1 ? "dark" : "outline-info"}> {toChart.dcrescente === 1 ? "Crescente" : "Decrescente"}</Button>


                    </Row>

                    <Row>

                        <Col>
                            <p>Setores</p>
                            <Setores />
                        </Col>

                        <Col >
                            <p>Papers </p>
                            <Form.Group >
                                <Form.Control
                                    autocomplete="off"
                                    type="text"
                                    autocomplete="off"
                                    onChange={handleChange}
                                    onKeyPress={RefilterRealTime}
                                    name="papeis"
                                    value={toChart.papeis || ""}
                                    placeholder="" />
                            </Form.Group>
                        </Col>

                        <Col sm={1}>
                            <p>Limite da lista</p>
                            <Form.Group >
                                <Form.Control autocomplete="off" type="number" autocomplete="off" onChange={handleChange} name="limite" value={toChart.limite || ""}
                                    placeholder="" />
                            </Form.Group>
                        </Col>

                        <Col>
                            <p>Classificar decrescente por</p>
                            <Classificar />
                        </Col>

                        <Col>
                            <p>Eixo Y</p>
                            <ListY />
                        </Col >
                        <Col sm="1">
                            <p> {`    R evert`}</p>
                            <Button onClick={revertAxis}> {'x'} </Button>
                        </Col>
                        <Col>
                            <p>Eixo X</p>
                            <ListX />
                        </Col>

                    </Row>

                    {/* <Acordeao> */}
                        {/* <Form.Group controlId="formBasicRangeCustom">
                            <Form.Label>{toChart.theange}</Form.Label>
                            <Form.Control defaultValue ="3" type="range" onChange={handleChange} name="theange" min="0" max="10"  step="0.0001" custom />
                            <Form.Control id="formBasicRangeCustom2" defaultValue ="8" type="range" onChange={handleChange} name="theange" min="0" max="10"  step="0.0001" custom />
                        </Form.Group> */}
                    {/* </Acordeao> */}

                    {/* <Grafico ysize={`${toChart.limite * 10 || 250}px`}> */}



{/* <DoubleRangeSlider/> */}

                    <Grafico >
                        <MyResponsiveScatterPlot
                        data={toChart.chartData} 
                        xlabel={toChart.Xaxis} 
                        ylabel={toChart.Yaxis} />
                    </Grafico>
                </StDiv2>
            </Container>
        </Layout>
    )
}

export default Destaquesv

const Grafico = styled.div`
    padding-left: 50px;
    /* margin-left: 50px; */
    height: ${props => props.ysize || '70vh'};
    width: 95vw;
    border:0.5px solid rgb(205, 209, 213); 
`

/**Div */
const StDiv2 = styled.div`
margin: 5px 0 25px 0;
padding: 25px 10px 10px 20px;
font-family: 'Heebo', sans-serif ;
border-radius : 5px;



.scroll {
  border-top:0.5px solid rgb(205, 209, 213); 
  height: 190px;
  overflow-x: hidden;
  overflow-y: auto;
 }

.marginb{   
    margin-bottom: 5px;
}

.centerme{   
   display:flex;
   align-items:center;
   justify-content:center;
}

.btn{
    margin: 5px 5px 15px 5px;
    min-width: 100px;
    border-top-left-radius: 9999px;
    border-top-right-radius: 9999px;
    border-bottom-right-radius: 9999px;
    border-bottom-left-radius: 9999px;
}

`

