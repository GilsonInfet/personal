import React, { useEffect, useState } from 'react'
import { Col, Row, Container, Button, Form, OverlayTrigger, Tooltip } from 'react-bootstrap'
import styled from 'styled-components'
import { getRealTimeRadar } from '../services/trader'
import Layout from './layout/layout'
import { StDiv } from './trade'
import { adicionaLST} from '../util/localsthandler'

const Quadro = () => {
    // const [fullData, setFullData] = useState(acao.opcoesAtmListCalculadas)

    const [noroeste, setNoroeste] = useState([])
    const [nordeste, setNordeste] = useState([])

    const [main, setMain] = useState("PETR4")

    const [mainObject, setMainObject] = useState({})

    const [sudoeste, setSudoeste] = useState([])
    const [sudeste, setSudeste] = useState([])

    const [fields, setFields] = useState({
        oeste: "pontosTecnicos",
        leste: "pontosFundamentais"
    })

    const [flag, setFlag] = useState("")

    const [mesmoSetor, setMesmoSetor] = useState(false)

    const ssetFlag = (item) => {


        if (flag === item) {
            setFlag("")
            setMain(item)
            // carregarUm()
        } else {
            setFlag(item)

        }

    }

    useEffect(
        () => {
            carregarUm()
        }, [fields.oeste, fields.leste, mesmoSetor, flag])


    const carregarUm = () => {


        const getdataOne = getRealTimeRadar()
            .then((res) => {


                loadKeys(res.data[0])
                let mainlocal = res.data.filter(item => item.TICKER == main)[0]
      
                if (!mainlocal) {
                    alert(`Papel ${main} não existe no radar`)
                    return
                }

                setMainObject(mainlocal)

                if (mesmoSetor) {
                    res.data = res.data.filter(item => item.setor === mainlocal.setor)
                }

                //LADO OESTE                
                let filtroNorOeste = res.data.filter(item => item[fields.oeste] > mainlocal[fields.oeste])
                setNoroeste(filtroNorOeste)

                let filtroSudOeste = res.data.filter(item => item[fields.oeste] < mainlocal[fields.oeste])
                setSudoeste(filtroSudOeste)



                //LESTE
                let filtroNordeste = res.data.filter(item => item[fields.leste] > mainlocal[fields.leste])
                setNordeste(filtroNordeste)

                let filtroSudeste = res.data.filter(item => item[fields.leste] < mainlocal[fields.leste])
                setSudeste(filtroSudeste)


            })

    }
    const [chaves, setChaves] = useState([])
    const handleChange = (attr) => {
        const { value, name } = attr.target

        setMain(value.toUpperCase())

    }

    const enterKeyPressed = (event) => {

        if (event.key == 'Enter') {
            carregarUm()
            adicionaLST(main)
        }
    }

    const handleChangeKeys = (attr) => {
        const { value, name } = attr.target

        setFields(
            {
                ...fields,
                [name]: value
            })

    }

    /** Não carrega chaves que são texto */
    const loadKeys = (objeto) => {
        for (const [key, value] of Object.entries(objeto)) {
            if (!isNaN(parseInt(value))) {
                if (!chaves.includes(key)) {
                    chaves.push(key)
                }
            }
        }
    }

    const CampoOeste = () => {
        return (
            <Form.Group >
                <Form.Control as="select" custom name="oeste" onChange={handleChangeKeys} value={fields.oeste}>

                    {chaves.map((chave, i) => (
                        <option key={i} value={chaves[i]}> {chave} </option>
                    ))}
                </Form.Control>
            </Form.Group>
        )
    }

    const CampoLeste = () => {
        return (
            <Form.Group >
                <Form.Control as="select" custom name="leste" onChange={handleChangeKeys} value={fields.leste}>

                    {chaves.map((chave, i) => (
                        <option key={i} value={chaves[i]}> {chave} </option>
                    ))}
                </Form.Control>
            </Form.Group>
        )
    }



    return (
        <>
            <Layout header={`Quadro`} footer={`Footer Quadro`}>
                <StDiv>
                    <Container fluid>


                        <Row>

                            <Col>
                                <Square>
                                    {noroeste.map((item, index) =>
                                        <Button
                                            onContextMenu={() => ssetFlag(item.TICKER)}
                                            onClick={() => ssetFlag(item.TICKER)}
                                            variant={item.TICKER === flag ? "success" : "secondary"}
                                            key={index}>
                                            {item.TICKER} / {item[fields.oeste].toFixed(2)}
                                        </Button>

                                    )}

                                </Square>
                            </Col>

                            <Col>
                                <Square>
                                    {nordeste.map((item, index) =>
                                        <Button
                                            onClick={() => ssetFlag(item.TICKER)}
                                            variant={item.TICKER === flag ? "success" : "secondary"}
                                            key={index}>
                                            {item.TICKER}  / {item[fields.leste]}
                                        </Button>)}
                                </Square>
                            </Col>

                        </Row>


                        <Envolve>

                            <Col>
                                <CampoOeste />
                            </Col>
                            <Col xs="1">
                                <Form.Control disabled type="text" name="aticker" value={mainObject[fields.oeste] || "0"} />
                            </Col>
                            <Col xs="auto">
                                <Button onClick={() => setMesmoSetor(!mesmoSetor)}
                                    variant={mesmoSetor ? "primary" : "success"}>
                                    {mesmoSetor ? "Todos Setores" : "Mesmo Setor"}
                                </Button>
                            </Col>
                            <Col xs="auto">
                                <Form.Control
                                    className="heebof"
                                    type="text"
                                    onChange={handleChange}
                                    onKeyPress={enterKeyPressed}
                                    name="aticker"
                                    value={main || ""} />

                            </Col>

                            <Col xs="auto">
                                <Button onClick={carregarUm} variant="primary"> Pesquisar</Button>
                            </Col>

                            <Col xs="1">
                                <Form.Control disabled type="text" name="aticker" value={mainObject[fields.leste] || "0"} />
                            </Col>
                            <Col>
                                <CampoLeste />
                            </Col>

                        </Envolve>


                        <Row>
                            <Col>
                                <Square>
                                    {sudoeste.map((item, index) =>
                                        <Button
                                            onClick={() => ssetFlag(item.TICKER)}
                                            variant={item.TICKER === flag ? "success" : "secondary"}
                                            key={index}>
                                            {item.TICKER} / {item[fields.oeste].toFixed(2)}
                                        </Button>)}
                                </Square>
                            </Col>

                            <Col>
                                <Square>
                                    {sudeste.map((item, index) => <Button
                                        onClick={() => ssetFlag(item.TICKER)}
                                        variant={item.TICKER === flag ? "success" : "secondary"}
                                        key={index}>
                                        {item.TICKER}  / {item[fields.leste].toFixed(2)}
                                    </Button>)}
                                </Square>
                            </Col>

                        </Row>


                    </Container>

                </StDiv>

            </Layout>
        </>
    )
}

export default Quadro


const Square = styled.div`
padding-top : 5px;
border-radius: 5px;
border:0.5px solid rgb(205, 209, 213); 
height: 38vh;
overflow-x: hidden;
overflow-y: auto;
width: 100%;
margin: 0px 0 15px 0;
justify-content: center;
button{
    min-width: 120px;
    border-top-left-radius: 9999px;
    border-top-right-radius: 9999px;
    border-bottom-right-radius: 9999px;
    border-bottom-left-radius: 9999px;
}
/* box-shadow: 5px 5px 5px 5px rgb(205, 209, 213) ; */

`

const Envolve = styled(Row)`
border-radius: 5px;
padding : 10px 0 1px 0;
margin: 0.5px 0.5px 10.5px 0.5px;
background-color : rgb( 0, 0, 0, .67);
border:0.5px solid rgb(205, 209, 213); 
`