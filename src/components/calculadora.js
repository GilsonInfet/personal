import React, { useEffect, useState } from 'react'
import { Col, Modal, Row, Container, Table, Button, Form, Spinner } from 'react-bootstrap'
import { getCalocOneOpcao, getFullData, getOpcoesDe, getRealTimeRadar } from '../services/trader'
import { saveToLocal } from '../util/savelocalCalc'
import Layout from './layout/layout'
import { useParams } from 'react-router-dom'
import { StDiv } from './trade'
import {localStoragem, adicionaLST} from '../util/localsthandler'
import { dadosuteis } from '../util/dadosUteis.js'
import ToTopButton from './topButton'
import { param } from 'jquery'

const Calculadora = () => {

    const { paper } = useParams()
    const [ticker, setTicker] = useState({ ticker: "loading" })
    const [opcaoData, setOpcaoData] = useState({ ticker: paper ? paper.toUpperCase() :  "" })
    const [allOptionsDe, setallOptionsDe] = useState([])
    const [allOptionsDeFiltrada, setallOptionsDeFiltrada] = useState([])

    const [fullData, setFullData] = useState({
        fulldata: "setFullData",
        radarRealtime: {
            qotacao: 0
        }
    })

    const dummyFiltros ={
        filtros: "setFiltros",
        fm: false,
        vencimentos: [],
        currentVencimento: ''
    }

    const [filtros, setFiltros] = useState(dummyFiltros)

const dummyresultado = {
    todo: "Todo",
    premioOpcao: 0,
    premioBase: 0
}

    const [resultado, setsResultado] = useState(dummyresultado)

    const [calculando, setcalculando] = useState(false);


    const aplicarFiltro = () => {
        console.log("chamada aplicar filtro", filtros)
        let temp = allOptionsDe

        if (filtros.marketMaker) {
            temp = temp.filter(item => item.marketMaker === filtros.marketMaker)
        }

        if (filtros.currentVencimento) {
            temp = temp.filter(item => filtros.currentVencimento.includes(item.vencimento))
        }


        setallOptionsDeFiltrada(temp)

    }

    useEffect(() => {

        aplicarFiltro()

    }, [filtros])



    const handleChange = (attr) => {
        const { value, name } = attr.target
        setOpcaoData({
            ...opcaoData,
            [name]: value.replace(",", ".").toUpperCase()
        })
    }


    const btnset = (attr) => {

        resetData()

        setOpcaoData({
            ...opcaoData,
            ticker: attr.replace(",", ".").toUpperCase()
        })
    }


    useEffect(
        () => {
            if (param) {
                getdataOfAtivo()
            }
        }
        , [])


    const enterKeyPressed = (event) => {

        if (event.key == 'Enter') {
            resetData()
            getdataOfAtivo()

        }
    }

const resetData = () => {
    setsResultado(dummyresultado)
    setTicker({ ticker: "loading" })
    // setFiltros(dummyFiltros)
}

    const getdataOfAtivo = async () => {

        let filtrar = opcaoData.ticker.substring(0, 4) || opcaoData.ativoTicker

        const ativo = dadosuteis.arrlistaTikersOpcoes
            .filter(item => item.includes(filtrar))[0]

        if (ativo) {
            adicionaLST(ativo.toUpperCase())
            const getdataOne = await getFullData({ TICKER: ativo })

                .then((res) => setFullData(res.data))
                .catch(res => alert('Erro em busca fulldata' + res))

            await PegaTodosOpcoesDe(ativo)
           
        } else {
            setFullData({
                ...fullData,
                erro: `Erro para ${opcaoData.ticker.substring(0, 4)}`
            })
        }
    }
    useEffect(
        () => {
            if (fullData) {
                setOpcaoData({
                    ...opcaoData,
                    fechamentoAnterior: fullData.radarRealtime.fechamentoAnterior
                })
            }
        }
        , [fullData])

    const PegaTodosOpcoesDe = async (ativo) => {
        setcalculando(true)
        const getdataOneOption = await getOpcoesDe({ optTicker: opcaoData.ticker.toUpperCase() })
        if (getdataOneOption.data.length > 0) {
            setTicker(getdataOneOption.data[0])
        }



        //Pega todas  opções correlacionadas
        const getdataOneAllOptions = await getOpcoesDe({ acao: ativo.toUpperCase() })
            .then((res) => {


                res.data = res.data.sort(function (a, b) {
                    if (a.strike > b.strike) {
                        return 1;
                    }
                    if (a.strike < b.strike) {
                        return -1;
                    }
                    return 0;
                })

                //Armazena os vencimentos das opções obtidas

                let tempVencimentos = []
                res.data.map(item => {
                    if (tempVencimentos.indexOf(item.vencimento) === -1) {
                        tempVencimentos.push(item.vencimento);
                    }
                })


                tempVencimentos = tempVencimentos.sort(function (a, b) {
                    if (a > b) {
                        return 1;
                    }
                    if (a < b) {
                        return -1;
                    }
                    return 0;
                })


                setFiltros({
                    ...filtros,
                    vencimentos: tempVencimentos
                })

               
                setallOptionsDe(res.data)

                setallOptionsDeFiltrada(res.data)
            })


            .catch(res => alert('Erro em busca allOptionsDe' + res))
            .finally(() => setcalculando(false))

    }

    const calculaUmaOpcao = async () => {


        if (!opcaoData.cotacao) {
            alert("Necessita informar Cotação do ativo a simular")
            return
        }



        // const fechamentoAnterior = fullData.yahoofinance.Fechamento
        const kueri = {
            cotacao: opcaoData.cotacao,
            ticker: opcaoData.ticker,
            // ativoTicker : opcaoData.ativoTicker, 
            cotacaoBase: fullData.radarRealtime.qotacao,
            fechamentoAnterior: opcaoData.fechamentoAnterior
        }

      
        setcalculando(true)
        const result = await getCalocOneOpcao(kueri)
            .then(res => {
                setsResultado(res.data)
            })
            .catch(res => {
            
                setsResultado({ erro: res.response.data })
            })
            .finally(res => {
                setcalculando(false)
            })

    }



    return (
        <>
            <Layout header={`Calculadora`}>
                <StDiv>
                    <Container>

                        <h4> Calculadora de prêmios estimados de opções - Black Scholes </h4>
                        <br />
                        <Row sm={12}>
                            <Col sm="4">

                                <Row >
                                    <Col>Ticker da opção </Col>
                                    <Col sm={7}>
                                        <Form.Control
                                            onKeyPress={enterKeyPressed}
                                            type="text" onChange={handleChange}
                                            className="heebof"
                                            name="ticker"
                                            placeholder="ticker da opção"
                                            value={opcaoData.ticker || ""} />


                                    </Col>
                                </Row>

                                <br />
                                <Row >
                                    <Col>Fechamento anterior do Ativo </Col>
                                    <Col sm={7}>
                                        <Form.Control

                                            type="text"
                                            disabled={true}

                                            className="heebof"
                                            name="fechamentoAnterior"
                                            placeholder="Cotação ativo dia anterior"
                                            value={opcaoData.fechamentoAnterior} />
                                    </Col>
                                </Row>
                                <br />
                                <Row >
                                    <Col>Cotação agora (Realtime Radar) </Col>
                                    <Col sm={7}>
                                        <Form.Control

                                            type="text"
                                            disabled={true}

                                            className="heebof"
                                            name="fechamentoAnterior"
                                            placeholder="Cotação ativo dia anterior"
                                            value={fullData.radarRealtime.qotacao} />
                                    </Col>
                                </Row>




                                <br />

                                <Row >
                                    <Col>Cotação do ativo a simular </Col>
                                    <Col sm={7}>
                                        <Form.Control
                                            onKeyPress={enterKeyPressed}
                                            type="text"
                                            onChange={handleChange}
                                            className="heebof"
                                            name="cotacao"
                                            placeholder="Cotação do ativo a simular"
                                            value={opcaoData.cotacao || ""} />
                                    </Col>
                                </Row>



                                <br />

                                <Button onClick={calculaUmaOpcao}> Calcular </Button>




                            </Col>





                            <Col sm="4">
                                {ticker.ativoPai ?
                                    <>
                                        <p>Ativo: {ticker.ativoPai}</p>
                                        <p>Ticker: {ticker.optTicker}</p>
                                        <p>Tipo: {ticker.tipo}</p>
                                        <p>Vencimento: {ticker.vencimento}</p>
                                        <p>Strike: {ticker.strike}</p>
                                        <p className={ticker.marketMaker ? "green" : "red"}  >Formador de Mercado: {ticker.marketMaker ? "SIM" : "NÃO"}</p>
                                        <p className={ticker.estilo == "AMER" ? "green" : "red"} >Estilo: {ticker.estilo}</p>
                                        <p>Último preço: {ticker.lastPric ? ticker.lastPric : "N/A"}</p>
                                    </>
                                    : ""}
                            </Col>

                            <Col sm="4">

                                <Row>

                                    {calculando ? <Spinner animation="border" size="sm" /> :
                                        <>
                                            {fullData.erro ? `Ativo para a opção ${opcaoData.ticker} não localizado` :
                                                <>
                                                    {Object.keys(resultado).length > 3 ?
                                                        <div>
                                                            <h4>Resultado para Opção  {resultado.ticker} : </h4>
                                                            <h4 className="btn-primary" >Prêmio Calculado: {resultado.premioOpcao.toFixed(2)}</h4>
                                                            <p> Cotação usada na simulação : {resultado.cotacaoFim}</p>
                                                            <hr />
                                                            <p>Cotação Now: {fullData.radarRealtime.qotacao} </p>
                                                            <p>Premio para cotação Now: {resultado.premioBase.toFixed(2)}</p>
                                                            <hr />
                                                            <p> Último prêmio do pregão anterior: {resultado.lastPremiox}</p>
                                                            <p> Fechamento Anterior do Ativo: {resultado.fechamentoAnterior}</p>

                                                        </div>
                                                        : <h4>Preencha os campos livres à esquerda e clique em calcular. O resultado aparecerá aqui.</h4>}
                                                </>
                                            }
                                        </>
                                    }
                                </Row>


                            </Col>


                        </Row>
                        <hr />
                        <Row>
                            <Col sm="2">
                                <Button
                                    variant={filtros.marketMaker ? "success" : "outline-success"}
                                    block
                                    onClick={() => {
                                        setFiltros({ ...filtros, marketMaker: !filtros.marketMaker })
                                    }
                                    }> Formador de Mercado </Button>

                                <Button
                                    block
                                    variant={!filtros.currentVencimento ? "success" : "outline-success"}
                                    onClick={() => {
                                        setFiltros({ ...filtros, currentVencimento: "" })
                                    }
                                    }> R Vencto </Button>

                            </Col>
                            <Col sm="10">
                                {calculando ? <Spinner animation="border" size="sm" /> :
                                    <>

                                        {filtros.vencimentos.map((item, index) =>
                                            <Button
                                                onClick={() => {
                                                    setFiltros({ ...filtros, currentVencimento: item })
                                                }}
                                                variant={filtros.currentVencimento == item ? "success" : "outline-success"}
                                                key={index}
                                                value={item}>
                                                {item}
                                            </Button>
                                        )}
                                    </>}
                            </Col>

                        </Row>

                        <hr />
                        <br />

                        <h5>Todas opções correlatas: {allOptionsDe.length} </h5>
                        <br />

                        <Row>
                            <Col>
                                <h5>Calls</h5>
                                <div className="scroll">
                                    {calculando ? <Spinner animation="border" size="sm" /> : ""}
                                    {allOptionsDeFiltrada.filter(item => item.tipo.includes("CALL")).map((item, i) =>
                                        <Row key={i}>
                                            <Button
                                                onClick={() => btnset(item.optTicker)}
                                                variant={item.marketMaker ? "success" : "secondary"} > {item.optTicker} </Button>
                                            <p className={fullData.radarRealtime.qotacao >= item.strike ? "green bold" : ""}>
                                                {item.marketMaker ? "FM_OK" : "NO_FM"}
                                             // {item.strike.toFixed(2)}
                                             // {item.vencimento}
                                             //{item.optTicker}
                                            </p>

                                        </Row>
                                    )}
                                </div>
                            </Col>

                            <Col>
                                <h5>Puts</h5>

                                <div className="scroll">
                                    {calculando ? <Spinner animation="border" size="sm" /> : ""}
                                    {allOptionsDeFiltrada.filter(item => item.tipo.includes("PUT")).map((item, i) =>

                                        <Row key={i}>
                                            <Button
                                                onClick={() => btnset(item.optTicker)}
                                                variant={item.marketMaker ? "success" : "secondary"} > {item.optTicker} </Button>
                                            <p className={fullData.radarRealtime.qotacao <= item.strike ? "green bold" : ""}> 
                                            {item.marketMaker ? "FM_OK" : "NO_FM"} //
                                             {item.strike.toFixed(2)} // 
                                             {item.vencimento} // 
                                             {item.optTicker}</p>

                                        </Row>

                                    )}
                                </div>
                            </Col>





                        </Row>

                    </Container>
                </StDiv>
                <ToTopButton />
            </Layout>

        </>
    )
}

export default Calculadora
