import React, { useState, useEffect } from 'react'
import { Col, Modal, Row, Container, Table, Button, Form, Spinner, OverlayTrigger, Tooltip } from 'react-bootstrap'
import styled from 'styled-components'
import { getFullData, getOpcoesDe, getCalocOneOpcao } from '../services/trader'
import optTipo from '../util/functioncallput'
import { acao } from '../util/objeto'
import { useParams } from 'react-router-dom'
import history from '../config/history';
import LinksGroup from './linksgroup'
import ToTopButton from './topButton'
import Layout from './layout/layout'
import { isBuy, TDisbuy } from './allOpcoes'

const Trade = () => {

    const { paper } = useParams()
    const [ticker, setTicker] = useState(paper || "")
    const [fullData, setFullData] = useState(acao)

    const [allOptionsDe, setallOptionsDe] = useState([{}])
    const [savedcalc, setsavedcalc] = useState([])

    const [calculando, setcalculando] = useState(false);
    const [show, setShow] = useState(false);
    const [modalText, setmodalText] = useState(<div></div>);

    const handleClose = () => {
        setShow(false)
        setcalculando(false)
    };
    const handleShow = () => setShow(true);


    const handleChange = (attr) => {
        const { value } = attr.target
        setTicker(value.toUpperCase())
    }


    const calculaUmaOpcao = async (cotacaoE, ticker, ativoTicker, strike, tipo) => {
        

        var cotacao
        let newCotacao = prompt(`Cotação do ativo para simular cálculo de ${ticker}:`, cotacaoE);



         
        if (newCotacao == null || newCotacao == "") {
            return
        } else {
            cotacao = newCotacao;
        }
        newCotacao = newCotacao.replace(",", ".")
        let message = ""
        if (tipo === "CALL") {
            if (strike > cotacao) {
                message = "CALL: O strike da opção proposta é MAIOR que o target da simulação "
            }
        } else {
            if (strike < cotacao) {
                message = "PUT: O strike da opção proposta é MENOR que o target da simulação "
            }
        }

        const cotacaoBase = cotacaoE
        const fechamentoAnterior = fullData.radarRealtime.fechamentoAnterior
        const kueri = { cotacao, ticker, ativoTicker, strike, cotacaoBase, fechamentoAnterior }
        setcalculando(true)
        const result = await getCalocOneOpcao(kueri)

        const mostraresult =
            <>
                <div>{ticker}:</div>
                <div>Simulado em R$ {cotacao} = R$  {result.data.premioOpcao.toFixed(2)}.</div>

                <div>Cotação Agora*: R$ {cotacaoBase} =  R$ {result.data.premioBase.toFixed(2)}. </div>

                {message ? <h1> {message} </h1> : ""}
            </>

        setmodalText(mostraresult)
      
        savedcalc.push({
            ticker,
            tipo,
            cotacaoBase: cotacaoE,
            premioBase: result.data.premioBase,
            cotacao: result.data.cotacaoReplaced,
            premio: result.data.premioOpcao,
            strike: result.data.strike_replaced,
            data: result.data.data
        })

        handleShow()
        saveToLocal("CALCULADOS", savedcalc)

    }

    const saveToLocal = (item, object) => {

        const jsonObj = JSON.stringify(object);
        localStorage.setItem(item, jsonObj);
    }

    const limparHistoricodeCalculados = () => {
        localStorage.setItem("CALCULADOS", JSON.stringify([]));
        loadIfLocal()
    }

    const loadIfLocal = () => {
        
        let str = localStorage.getItem("CALCULADOS");

        if (!str) {
            localStorage.setItem("CALCULADOS", JSON.stringify([]));
            str = localStorage.getItem("CALCULADOS");
        }

        const parsedObj = JSON.parse(str)
        setsavedcalc(parsedObj)
    }

    const PegaTodosRealtime = async () => {
        
        await getFullData({ TICKER: ticker.toUpperCase() })
            .then((res) => setFullData(res.data))
            .catch(res => alert('Erro em busca fulldata' + res))
        
        await getOpcoesDe({ acao: ticker.toUpperCase() })
            .then((res) => {

                
                res.data = res.data.sort(function (a, b) {
                    if (a.strike > b.strike) {
                        return 1;
                    }
                    if (a.strike < b.strike) {
                        return -1;
                    }
                    return 0;
                })
                setallOptionsDe(res.data)
            })


            .catch(res => alert('Erro em busca allOptionsDe' + res))

    }
    /**Retorna o p´roximo múltiplo de 100 do lote ótimo */
    function loteOtimo(dinheiro, premio) {
        return Math.ceil((dinheiro / premio) / 100) * 100
    }
    useEffect(
        () => {
            loadIfLocal()
            if (paper) {
                PegaTodosRealtime()
            }
        }, [])


    const MyModal = () => {
        return (
            <>


                <Modal show={show} onHide={handleClose} animation={false}>
                    <Modal.Header closeButton>
                        <Modal.Title>Resultado do cálculo.</Modal.Title>
                    </Modal.Header>
                    <Modal.Body  >
                        <p>
                            {modalText}
                        </p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                  </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }

    const RadarRealtime = () => {
        return (
            <>
                <h4 >FAST DATA - {fullData.fundamentos.TICKER} (dados de checkmm9, delayed)</h4>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>Cotação</th>
                            <th>Delta Preço</th>
                            <th>Delta Vol </th>
                            <th>DY Agora</th>
                            <th>$/MM9</th>
                            <th>$/MM20</th>
                            <th>$/MM45</th>
                            <th>Data Report</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> {fullData.radarRealtime.qotacao}</td>
                            <td> {fullData.radarRealtime.deltaPreco}</td>
                            <td> {fullData.radarRealtime.deltaVol}</td>
                            <td>{fullData.radarRealtime.dyNow}</td>
                            <td>{fullData.radarRealtime.atualPorMM9}</td>
                            <td>{fullData.radarRealtime.atualPorMM20}</td>
                            <td>{fullData.radarRealtime.atualPorMM45}</td>
                            <td>{fullData.radarRealtime.dataPesquisa}</td>
                        </tr>


                    </tbody>
                </Table>
                <br /><br /><br />
            </>
        )
    }



    const Yahoofinance = () => {
        return (
            <>
                <h4>DADOS D - 1 - {fullData.fundamentos.TICKER}</h4>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>Abertura</th>
                            <th>Máxima</th>
                            <th>Mínima </th>
                            <th>Fechamento</th>
                            <th>Vol Médio Dia</th>
                            <th>Data Report</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            v                            <td> {fullData.yahoofinance.Abrir}</td>
                            <td> {fullData.yahoofinance.Alto}</td>
                            <td> {fullData.yahoofinance.Baixo}</td>
                            <td>{fullData.yahoofinance.Fechamento}</td>
                            <td>{fullData.yahoofinance.volmedioDia}</td>
                            <td>{fullData.yahoofinance.dataPesquisa}</td>

                        </tr>


                    </tbody>
                </Table>
                <br /><br /><br />
            </>
        )
    }


    const Fundamentus = () => {
        return (
            <>
                <h4>FUNDAMENTUS -{fullData.fundamentos.TICKER} </h4>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>Preço/Lucro</th>
                            <th>P/VP</th>
                            <th>DY </th>
                            <th>Margem EBTD</th>
                            <th>Margem Líq</th>
                            <th>Liq Corr</th>
                            <th>ROE</th>
                            <th>Cres Receita</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> {fullData.fundamentos.PL}</td>
                            <td> {fullData.fundamentos.PVP}</td>
                            <td> {fullData.fundamentos.DivYield}</td>
                            <td> {fullData.fundamentos.MrgEbit}</td>
                            <td> {fullData.fundamentos.MrgLiq}</td>
                            <td> {fullData.fundamentos.LiqCorr}</td>
                            <td> {fullData.fundamentos.ROE}</td>
                            <td> {fullData.fundamentos.CrescRec5a}</td>
                        </tr>


                    </tbody>
                </Table>
                <br /><br /><br />
            </>
        )
    }

    const OpcoesAtmCalculadas = () => {
        return (
            <>

                <OverlayTrigger overlay={<Tooltip id={`tooltip-Atm`}>
                    <>
                        Fonte relatório de checkmm9, reativo ao excel
                    </>
                </Tooltip>}>
                    <h4>OPÇÕES ATM LISTADAS E CALCULADAS - {fullData.fundamentos.TICKER}</h4>
                </OverlayTrigger>

                <br />

                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>Ticker da Opção</th>
                            <th>Tipo</th>
                            <th>Prêmio Estimado </th>
                            <th>Strike</th>
                            <th>Cotação</th>
                            <th>Strike/Cotação</th>
                            <th>Momento do cálculo</th>
                            <th>Links</th>
                        </tr>
                    </thead>
                    <tbody>

                        {fullData.opcoesAtmListCalculadas.map(item => (
                            <tr>
                                <td> {item.optTicker}</td>
                                <td> {optTipo(item.optTicker)}</td>
                                <td> {item.premio}</td>
                                <td> {item.strike}</td>
                                <td> {item.coacaoAtivo}</td>
                                <td> {(((item.strike / item.coacaoAtivo) - 1) * 100).toFixed(2)}%</td>
                                <td > {item.data}</td>
                                <td className="centerme">
                                    <LinksGroup ticker={{
                                        optTicker: item.optTicker,
                                        TICKER: item.TICKER
                                    }} />
                                </td>
                            </tr>
                        ))}

                    </tbody>
                </Table>


            </>
        )
    }

    const DemaisOpcoesDe = () => {
        return (
            <>

<OverlayTrigger overlay={<Tooltip id={`tooltip-Atm`}>
                    <>
                        Fonte relatório busca.csv, 
                    </>
                </Tooltip>}>
                <h4>DEMAIS OPÇÕES DE - {fullData.fundamentos.TICKER} - {allOptionsDe.length} papeis</h4>
                </OverlayTrigger>

                

                <br />

                <div className="no-scroll">
                    <Table striped bordered hover >
                        <thead>
                            <tr>
                                <th className="tdcol6">Ticker da Opção</th>
                                <th className="tdcol6">Tipo</th>

                                <th className="tdcol6">Strike</th>
                                <th className="tdcol6">Strike/Cotação</th>
                                <th className="tdcol6">Calcular</th>
                                <th className="tdcol6">Links</th>
                            </tr>
                        </thead>
                        {/* <tbody></tbody> */}
                    </Table>
                </div>

                <div className="scroll">

                    <Table striped bordered hover >
                        <thead>
                            <tr>      </tr>
                        </thead>
                        <tbody>

                            {allOptionsDe.map(item => (
                                <tr>
                                    <TDisbuy buy={isBuy(
                                        {
                                            optTicker: item.optTicker,
                                            strike: item.strike,
                                            coacaoAtivo: fullData.radarRealtime.qotacao
                                        })
                                    } className="tdcol6"> {item.optTicker}</TDisbuy>
                                    <td className="tdcol6"> {optTipo(item.optTicker)}</td>
                                    <td className="tdcol6"> {item.strike}</td>
                                    <td className="tdcol6"> {(((item.strike / fullData.radarRealtime.qotacao) - 1) * 100).toFixed(2)}%</td>
                                    <td className="tdcol6" >
                                        <Button variant="outline-info"
                                            onClick={() => calculaUmaOpcao(fullData.radarRealtime.qotacao, item.optTicker, ticker, item.strike, optTipo(item.optTicker))}>
                                            {calculando ? <Spinner animation="border" size="sm" /> : "Calcular"}

                                        </Button>
                                    </td>
                                    <td className="centerme tdcol6">
                                        <LinksGroup ticker={{
                                            optTicker: item.optTicker,
                                            TICKER: item.TICKER
                                        }} />
                                    </td>
                                </tr>
                            ))}

                        </tbody>
                    </Table>
                </div>

            </>
        )
    }



    const DemaisOpcoesDeCalculedlist = () => {
        return (
            <>


                <h4>Histórico de Cálculos -  {savedcalc.length} itens</h4>
                <Button className="ml-auto" variant="danger" onClick={limparHistoricodeCalculados}>Limpar Histórico De Cálculos</Button>


                <br />

                <div className="no-scroll ">
                    <Table striped bordered hover >
                        <thead>
                            <tr>
                                <th className="tdcol6">Ticker da Opção</th>
                                <th className="tdcol6">Tipo</th>
                                <th className="tdcol6">Strike</th>
                                <th className="tdcol6">Cotação Base*</th>
                                <th className="tdcol6">Prêmio Base*</th>
                                <th className="tdcol6">Lote Base*</th>
                                <th className="tdcol6">Invest Base</th>
                                <th className="tdcol6">Cotação simulada</th>
                                <th className="tdcol6">Prêmio Simulado</th>
                                {/* <th className="tdcol6">Lote p $500</th> */}
                                <th className="tdcol6">Prêmio Simulado x Lote</th>
                                <th className="tdcol6">Delta Prêmio</th>
                                <th className="tdcol6">Líquido</th>
                            </tr>
                        </thead>
                        {/* <tbody></tbody> */}
                    </Table>
                </div>

                {savedcalc.length > 0 ?
                    <div className="scroll">

                        <Table striped bordered hover >
                            <thead>
                                <tr>

                                </tr>
                            </thead>
                            <tbody>

                                {savedcalc.map(item => (


                                    <tr>
                                        <td className="tdcol6"> {item.ticker}</td>
                                        <td className="tdcol6"> {item.tipo}</td>
                                        <td className="tdcol6">R$  {item.strike.toFixed(2)}</td>

                                        <td className="tdcol6"> R$  {item.cotacaoBase}          </td>
                                        <td className="tdcol6"> R$  {item.premioBase.toFixed(2)}         </td>
                                        <td className="tdcol6">  {loteOtimo(500, item.premioBase)} </td>
                                        <td className="tdcol6"> R$ {(loteOtimo(500, item.premioBase) * item.premioBase).toFixed(2)} </td>
                                        <td className="tdcol6"> R$  {item.cotacao.toFixed(2)}          </td>
                                        <td className="tdcol6"> R$  {item.premio.toFixed(2)}         </td>
                                        {/* <td className="tdcol6">  {loteOtimo(500, item.premio)} </td> */}
                                        <td className="tdcol6"> R$ {(item.premio * loteOtimo(500, item.premioBase)).toFixed(2)} </td>

                                        <td className="tdcol6">  {(((item.premio / item.premioBase) - 1) * 100).toFixed(2)} % </td>
                                        <td className="tdcol6"> {resutLiq((loteOtimo(500, item.premioBase) * item.premioBase), (item.premio * loteOtimo(500, item.premioBase)))}   </td>
                                    </tr>
                                ))}

                            </tbody>
                        </Table>
                    </div>
                    : <> <br /> <hr /><br /></>}

            </>
        )
    }
    const RsiData = () => {
        return (
            <>
                <h4 >RSI DATA {fullData.fundamentos.TICKER}</h4>

                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>AÇÃO</th>
                            <th>RSI</th>
                            <th>RSI_ST_FAST </th>
                            <th>Data Report</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> {fullData.rsiData.TICKER}</td>
                            <td>{fullData.rsiData.RSI}</td>
                            <td>{fullData.rsiData.RSI_ST_FAST}</td>
                            <td>{fullData.rsiData.dataPesquisa}</td>
                        </tr>


                    </tbody>
                </Table>

            </>
        )
    }



    function enterKeyPressed(event) {

        if (event.key == 'Enter') {
            if (ticker) {
                PegaTodosRealtime()
            }
            return true;
        }
    }

    function resutLiq(inn, outt) {
        let taxas = (inn + outt) * 0.0025 + 20
        let impostoSwing = (outt - inn) * 0.15
        let impostoDaytrade = (outt - inn) * 0.20
        let resultSwing = outt - inn - taxas - impostoSwing
        let resultDT = outt - inn - taxas - impostoDaytrade
        return (<>
            <div>DT: R$ {resultDT.toFixed(2)} </div>
            <div>ST: R$ {resultSwing.toFixed(2)}</div>
        </>)


    }

    return (

        <Container fluid>

            <Layout header={`Trading Search ${fullData.fundamentos.TICKER}`}>
                {/* <Destaques /> */}
                <MyModal />

                <StDiv>
                    <Row>
                        <Col>
                            <Form.Control onKeyPress={enterKeyPressed} className="heebof" type="text" onChange={handleChange} name="ticker" value={ticker.toUpperCase() || ""} />
                        </Col>
                        <Col>
                            <Button disabled={!ticker} onClick={PegaTodosRealtime}> Pesquisar</Button>
                        </Col>

                        <Col md="5">
                            <a target="_blank" rel="noopener noreferrer"
                                href={`https://www.fundamentus.com.br/detalhes.php?papel=${ticker}`}
                                class="btn btn-success"
                                role="button">Fundamentus </a>

                            <a target="_blank" rel="noopener noreferrer"
                                href={`https://www.tradingview.com/symbols/BMFBOVESPA-${ticker}/technicals/`}
                                class="btn btn-primary"
                                role="button">Trading View </a>

                            <a target="_blank" rel="noopener noreferrer"
                                href={`https://opcoes.net.br/calculadora-Black-Scholes/${ticker}`}
                                class="btn btn-dark"
                                role="button">Scholes </a>

                            <a target="_blank" rel="noopener noreferrer"
                                href={`https://twitter.com/search?q=%23${ticker}&src=typed_query&f=live`}
                                class="btn btn-primary"
                                role="button">Twitter </a>

                            <Button
                                onClick={() => history.push('/alloptions')}
                                variant="outline-primary"
                                role="button">All Options </Button>

                            <Button
                                onClick={() => history.push('/grafico')}
                                variant="outline-danger"
                                role="button">Grafico </Button>
                        </Col>

                    </Row>

                    <br /><br />
                    <Row>

                        <br /><br />
                        <RadarRealtime />
                        <br /><br />

                        <OpcoesAtmCalculadas />
                        <br /><br />
                        <br /><br />
                        <br /><br />
                        <DemaisOpcoesDe />
                        <br /><br />

                        <DemaisOpcoesDeCalculedlist />
                        {/* <iframe width="1024" height="768" src="https://www.tradingview.com/symbols/BMFBOVESPA-BRKM5/"></iframe> */}
                        <br /><br />

                        {/* <Yahoofinance /> */}
                        <br /><br />

                        <RsiData />

                        <br /><br /><br /><br /><br /><br />


                        <Fundamentus />

                    </Row>

                </StDiv>
                <ToTopButton />
            </Layout>
        </Container>
    )
}

export default Trade


/**Div */
export const StDiv = styled.div`
background-color: white;
min-height: 100vh;
padding: 20px;
/* font-family: Arial, Helvetica, sans-serif; */
font-family: 'Heebo', sans-serif ;
transition: 1000ms;

.btn{
margin: 0 5px 5px 5px;
min-width: 100px;
}



h4, th, td{
    font-family: 'Heebo', sans-serif ;
}
.center{
    background-size: 100% 100%;  //ajusta as dimensões da imagem às da DIV
    display:flex;
align-items:center;
justify-content:center;
transition : 1000ms;
width: 250px;
height: 360px;
}

.centerme{   
   display:flex;
   align-items:center;
   justify-content:center;
}

.scroll {
border-top:0.5px solid rgb(205, 209, 213); 
height: 250px;
overflow-x: hidden;
overflow-y: auto;
width: 100%;
margin: -10px 0 60px 0;
box-shadow: 5px 5px 5px 5px rgb(205, 209, 213) ;

th, td {
    width: 20vw; 
}

}

.no-scroll{
    width:95vw;
    th  {
    width: 20%;
    border: none;
    text-align:center;
}
.tdcol6{
    width: 8.33vw; 
}

.red{
    color: red;
    font-weight: 900;
}

}`
/* #root > div > div.sc-vUDtZ.fuaCwZ > div:nth-child(4) > table:nth-child(17) > thead > tr > th:nth-child(1) */





