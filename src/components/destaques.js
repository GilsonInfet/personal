import React, { useEffect, useState } from 'react'
import { Row, Container, Table, Button, OverlayTrigger, Tooltip, Carousel } from 'react-bootstrap'
import styled from 'styled-components'
import { getRealTimeRadar, suspiciousVolumes, bollinger  , aluguel} from '../services/trader'
import { adicionaLST } from '../util/localsthandler'
import { acao } from '../util/objeto'
import history from '../config/history';
import LinksGroup from './linksgroup'
import { STTable } from './comparador'

//https://scotch.io/bar-talk/convert-class-components-to-functional-components-in-a-react-project-solution-to-code-challenge-14

/**Lê radarRealtime: `cache/radarRealtime.csv` */
const Destaques = (props) => {

    const [aluguelst, setAlugelst]= useState([{}])
    const [bolBands, setbolBands] = useState([{}])
    const [fullData, setFullData] = useState(acao.opcoesAtmListCalculadas)
    const [volSus, setVolSus] = useState([{
        optTickerSister: "VIIAV960",
        optTicker: "VIIAJ960",
        serie: "J",
        TckrSymb: "VIIAJ960",
        marketMaker: true,
        tipo: "CALL",
        estilo: "AMER",
        ativoPai: "VIIA3",
        strike: 9.6,
        lastPric: 0.83,
        dataRelat: "2021-09-03",
        vencimento: "2021-10-15",
        CvrdQty: 0,
        TtlBlckdPos: 0,
        UcvrdQty: 0,
        TtlPos: 0,
        BrrwrQty: 0,
        fathersAssetLastPrice: 9.62,
        inOutAtMoney: "ITM",
        diffVolNeighboors: 10.37,
        tradQty: 508,
        YesterdayBfreYesterday: 508,
        keeped: [
            "dataRelat",
            "vencimento",
        ]
    }])



    const [destaqueVol, setDestaqueVol] = useState([])
    const [destaquePrice, setDestaquePrice] = useState([])
    const [destaqueLowPrice, setDestaqueLowPrice] = useState([])
    const [destaqueMM9, setDestaqueMM9] = useState([])
    const [lpapvp, setlpapvp] = useState([])


    const [fastOtimo, setFastOtimo] = useState([])
    const [cruzou, setCruzou] = useState([])
    const [otimoHold, setOtimoHold] = useState([])
    const [melhorRSI, setMelhorRSI] = useState([])
    const [melhorFundamentos, setMelhorFundamentos] = useState([])
    const [melhorqualPrecoFund, setMelhorqualPrecoFund] = useState([])
    const [melhorqualSaudeFund, setMelhorqualSaudeFund] = useState([])
    //qualSaudeFund

    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };

    const limiteDistMM9 = 0.01

    const carregarUm = async () => {

        

        await aluguel()
        .then((res) => {
            debugger
            setAlugelst(res.data)
        })

        await suspiciousVolumes()
            .then((res) => {
                setVolSus(res.data)
            })

        await bollinger()
            .then((res) => {
                setbolBands(res.data)
            })

        await getRealTimeRadar()
            .then((res) => {
                // res.data.map(item => item.ascMM9 = item.ascMM9)
                res.data.map(item => item.atualPorMM9d = Math.abs(1 - item.atualPorMM9))

                setFullData(res.data)

                res.data = res.data.sort(function (a, b) {
                    if (a.deltaVol < b.deltaVol) {
                        return 1;
                    }
                    if (a.deltaVol > b.deltaVol) {
                        return -1;
                    }
                    return 0;
                })
                setDestaqueVol(res.data.slice(0, 10))


                let Price = res.data.sort(function (a, b) {
                    if (a.deltaPreco < b.deltaPreco) {
                        return 1;
                    }
                    if (a.deltaPreco > b.deltaPreco) {
                        return -1;
                    }
                    return 0;
                })

                setDestaquePrice(Price.slice(0, 10))

                let LowPrice = res.data.sort(function (a, b) {
                    if (a.deltaPreco < b.deltaPreco) {
                        return -1;
                    }
                    if (a.deltaPreco > b.deltaPreco) {
                        return 1;
                    }
                    return 0;
                })

                setDestaqueLowPrice(LowPrice.slice(0, 10))

                let Mm9 = res.data.filter(item => item.atualPorMM9d <= limiteDistMM9)
                    .filter(item => item.atualPorMM9 >= 1)
                    .filter(item => item.ascMM9 > 0.50)
                    .sort(function (a, b) {
                        if (a.atualPorMM9 > b.atualPorMM9) {
                            return 1;
                        }
                        if (a.atualPorMM9 < b.atualPorMM9) {
                            return -1;
                        }
                        return 0;
                    })

                setDestaqueMM9(Mm9.slice(0, 10))

                let Cruzados = res.data.filter(item => item.cruzamento)
                    .filter(item => item.cruzamento.includes("ruzou"))
                setCruzou(Cruzados)

                //Seleção Ótima
                let otimoSwingt = res.data.filter(item => item.theGap > 0.005)
                    .filter(item => item.deltaVol > 0.9)
                    .filter(item => item.deltaPreco > 0.01) //
                    .slice(0, 20)
                    .sort(function (a, b) {
                        if (a.ascMM9 > b.ascMM9) { return 1 }
                        if (a.ascMM9 < b.ascMM9) { return -1 }
                        return 0;
                    })
                //Seleção Ótima
                setFastOtimo(otimoSwingt)


                /**ótimop holdar */
                let otimoHoldar = res.data.filter(item => item.dyNow > 0)
                otimoHoldar = otimoHoldar.filter(item => item.dividaLiqToNode <= 0)
                // otimoHoldar = otimoHoldar.filter(item => item.rsiToNode <= 40)
                otimoHoldar = otimoHoldar.filter(item => item.lpa > 0)
                otimoHoldar = otimoHoldar.filter(item => item.precoPorLucro > 0 && item.precoPorLucro <= 36.36)
                otimoHoldar = otimoHoldar.filter(item => item.pvpnow > 0 && item.pvpnow <= 3)
                otimoHoldar = otimoHoldar.filter(item => item.qotacao <= item.vlrIntrinseco)

                setOtimoHold(otimoHoldar)


                let bestRSI = res.data.sort(function (a, b) {

                    if (a.rsiMedia > b.rsiMedia) {
                        return 1;
                    }
                    if (a.rsiMedia < b.rsiMedia) {
                        return -1;
                    }

                    return 0;
                }).slice(0, 18)

                setMelhorRSI(bestRSI)

                let lpapvp = res.data.sort(function (a, b) {

                    if (a.lpaSobrePvp < b.lpaSobrePvp) {
                        return 1;
                    }
                    if (a.lpaSobrePvp > b.lpaSobrePvp) {
                        return -1;
                    }

                    return 0;
                }).filter(item => item.lpaSobrePvp > 0)
                    .filter(item => item.pvpnow < 1)

                setlpapvp(lpapvp)


                let bestFundamentais = res.data.sort(function (a, b) {

                    if (a.pontosFundamentais > b.pontosFundamentais) {
                        return -1;
                    }
                    if (a.pontosFundamentais < b.pontosFundamentais) {
                        return 1;
                    }

                    return 0;
                }).slice(0, 18)

                setMelhorFundamentos(bestFundamentais)


                let qualPrecoFund = res.data.sort(function (a, b) {

                    if (a.qualPrecoFund > b.qualPrecoFund) {
                        return -1;
                    }
                    if (a.qualPrecoFund < b.qualPrecoFund) {
                        return 1;
                    }
                    return 0;
                }).slice(0, 18)

                setMelhorqualPrecoFund(qualPrecoFund)

                let qualSaudeFund = res.data.sort(function (a, b) {

                    if (a.qualSaudeFund > b.qualSaudeFund) {
                        return -1;
                    }
                    if (a.qualSaudeFund < b.qualSaudeFund) {
                        return 1;
                    }
                    return 0;
                }).slice(0, 18)

                setMelhorqualSaudeFund(qualSaudeFund)

            })

    }




    useEffect(
        () => {
            carregarUm()
        }, [])


    useEffect(() => {
        setInterval(() => {
            //ponha aqui a função que irá atualizar seu state
            carregarUm()
        }, 5 * 60 * 1000)
    }, []);


    const goAndUpdate = (TICKER) => {
        history.push(`/trade/${TICKER}`)
        window.location.reload()
    }



    const Monttable = (props) => {


        return (
            <>
                <br />
                <h4>Monttable: {props.label} </h4>
                <h4>Hora Report: {fullData[0]?.dataPesquisa} </h4>
                <br />
                <div className="montttable" >
                    <Table striped bordered hover >
                        <thead>
                            <tr>
                                <th>Ticker </th>
                                {props.exibeColuna ? <th>{props.exibeColuna} </th> : ""}
                                <th>Cotação</th>
                                <th>D$ % </th>
                                <th>Persist MM9</th>
                                <th>DY Now</th>
                                <th>D Vol</th>
                                <th>Distância MM9</th>
                                <th>Opções</th>
                                <th>Links</th>
                            </tr>
                        </thead>

                        <tbody>

                            {props.tabela.map(item => (
                                <tr>
                                    <td> {item.TICKER}</td>
                                    {/* <td> <Button variant="link" onClick={() => goAndUpdate(item.TICKER)} >{item.TICKER}</Button></td> */}
                                    {props.exibeColuna ? <td>{item[props.exibeColuna]} </td> : ""}
                                    <td> {item.qotacao.toFixed(2)}</td>
                                    <td> {(item.deltaPreco * 100).toFixed(2)}</td>
                                    <td> {(item.ascMM9 - 0.25) / 0.25} </td>
                                    <td> {(item.dyNow * 100).toFixed(2)}%</td>
                                    <td> {item.deltaVol}</td>
                                    <td>{((item.atualPorMM9 - 1) * 100).toFixed(2)}</td>
                                    <td> {item.opcoesAtmSugeridas}</td>
                                    <td>
                                        <LinksGroup onClick={() => adicionaLST(item.TICKER)} ticker={{

                                            TICKER: item.TICKER
                                        }} />

                                    </td>
                                </tr>
                            ))}

                        </tbody>

                    </Table>
                    {/* </div> */}
                </div>
                <br />
                <hr />
            </>
        )
    }



    /**Ticker e links button sempre aparecem na extremidade, logo não precisa por no array.
     * 
     */
    const MonttableArr = (props) => {

        const defaultFields = props.colunas ? props.colunas : ['Ticker']
 
        return (
            <>
                <br />
                <h4>{props.label} </h4>
                <h4> MonttableArr - Hora Report: {fullData[0]?.dataPesquisa} </h4>
                <br />
                <div className="montttable" >
                    <STTable striped bordered hover >
                        <thead>
                            <tr>
                                <th>Ticker</th>
                                {defaultFields.map(item =>


                                    <OverlayTrigger overlay={<Tooltip id={`tooltip-disabled-v${item}`}>
                                        <>
                                            {item}
                                        </>
                                    </Tooltip>}>
                                        <th>{item}</th>

                                    </OverlayTrigger>

                                )}

                                <th>Opções</th>
                                <th>Links</th>
                            </tr>
                        </thead>

                        <tbody className="no-scroll">

                            {props.tabela.map(item => (
                                <tr>
                                    <td> {item.TICKER || item.ticker}</td>


                                    {defaultFields.map(itemf => (<td> {item[itemf]} </td>))}
                                    <td> {item.opcoesAtmSugeridas}</td>
                                    <td>
                                        <LinksGroup ticker={{

                                            TICKER: item.TICKER
                                        }} />

                                    </td>


                                </tr>
                            ))}

                        </tbody>

                    </STTable>
                    {/* </div> */}
                </div>
                <br />
                <hr />
            </>
        )
    }


    return (

        <StDiv2>
            <Container>
            </Container>

            {props.tabela ?
                <Container>
                    <h4>Hora Report: {fullData[0]?.dataPesquisa} </h4>


                    <section id="aluguel"> </section>
                    <MonttableArr tabela={aluguelst}
                        label={'Assets de rank de aluguel'}
                        colunas={['aluguelFreeFloatRatio', 'acoesAlugadasAgora', 'qtdAcoes', 'qotacao', 'TradAvrgPric', 'diffQotationAvgPrice', 'diffTotal', 'qtdAcoesFreeFloat']}
                    />

                    <section id="bollinger"> </section>
                    <MonttableArr tabela={bolBands}
                        label={'Assets que estouraram a BB'}
                        colunas={['direction', 'closingPrice', 'directionLimitBand', 'diference', 'timeRelat']}
                    />

                    <section id="volSus"> </section>
                    <MonttableArr tabela={volSus}
                        label={'Volumes Suspeitos de Opções'}
                        colunas={['optTicker', 'optTickerSister', 'inOutAtMoney', 'diffVolNeighboors', 'YesterdayBfreYesterday', 'tradQty']}
                    />

                    <section id="destaqueMM9"> </section>
                    <MonttableArr tabela={destaqueMM9}
                        label={'Mais próximos e acima de MM9'}
                        colunas={['qotacao', 'deltaPreco', 'deltaVol', 'mm9distAbs', 'ascMM9']}
                    />

                    <section id="melhorRSI"> </section>
                    <MonttableArr tabela={melhorRSI}
                        label={'Menores RSI'}
                        colunas={['qotacao', 'rsiToNode', 'rsi_storch_fast', 'rsiMedia', 'rsimdeltapreco']}
                    />


                    <section id="lpapvp"> </section>
                    <MonttableArr tabela={lpapvp}
                        label={'Melhores LPA e PVP'}
                        colunas={['qotacao', 'lpaSobrePvp', 'lpa', 'pvpnow', 'dyNow', 'mm9distAbs', 'rsiMedia']}
                    />

                    <section id="otimoHold"> </section>
                    <Monttable tabela={otimoHold} label={'Longs sugeridos'} />

                    <section id="fastOtimo"> </section>
                    <MonttableArr tabela={fastOtimo}
                        label={'PVG'}
                        colunas={['qotacao', 'deltaPreco', 'deltaVol', 'theGap', 'opcoesAtmSugeridas']}
                    />


                    <section id="cruzou"> </section>
                    <Monttable tabela={cruzou} label={'Cruzamentos de MM9 entre ontem e hoje.'} />

                    <section id="destaqueVol"> </section>
                    <Monttable tabela={destaqueVol} label={'Destaques de Volume'} />

                    <section id="destaquePrice"> </section>
                    <Monttable tabela={destaquePrice} label={'Maiores Altas'} />

                    <section id="destaqueLowPrice"> </section>
                    <Monttable tabela={destaqueLowPrice} label={'Maiores Baixas'} />

                    <section id="melhorFundamentos"> </section>
                    <Monttable tabela={melhorFundamentos} label={'Pontuação geral de Fundamentos'}
                        exibeColuna={'pontosFundamentais'} />


                    <section id="melhorqualPrecoFund"> </section>
                    <Monttable
                        tabela={melhorqualPrecoFund}
                        label={'Melhores preços segundo Fundamentos'}
                        exibeColuna={'qualPrecoFund'}
                        colunas={['qotacao', 'deltaPreco', 'precoPorLucro', 'lpa', 'pvpnow', 'vlrIntrinsecoPorCotacao', 'opcoesAtmSugeridas']}
                    />

                    <section id="qualSaudeFund"> </section>
                    <Monttable tabela={melhorqualSaudeFund}
                        label={'Melhores em saúde conforme Fundamentos'}
                        exibeColuna={'qualSaudeFund'}
                    />




                    <SecLink>
                    
                        <a href="#aluguel">ALUGUEL</a>
                        <a href="#bollinger">BOLLINGER</a>
                        <a href="#volSus">VOL SUSP OPÇÕES</a>
                        <a href="#fastOtimo">PVG</a>
                        <a href="#destaqueMM9">MM9</a>
                        <a href="#melhorRSI">RSI BAIXO</a>
                        <a href="#lpapvp">MELHOR LPA e PVP</a>


                        <a href="#otimoHold">LONG</a>
                        <a href="#destaqueVol">VOLUME</a>

                        <a href="#cruzou">CRUZOU</a>
                        <a href="#destaqueLowPrice">BAIXAS</a>
                        <a href="#destaquePrice">ALTAS</a>
                        <a href="#melhorFundamentos">FUNDAMENTOS BEST</a>
                        <a href="#melhorqualPrecoFund">QUALIDADE PRICE/FUND</a>
                        <a href="#qualSaudeFund">QUALIDADE SAUDE FUND</a>
                    </SecLink>
                </Container>
                : ""}

        </StDiv2>

    )
}

export default Destaques

const SecLink = styled.div`
display:flex;
flex-direction:column;
position: fixed; /* Fixed/sticky position */

    top: 200px; /* Place the button at the bottom of the page */
    left: 30px; /* Place the button 30px from the right */

    z-index: 99; /* Make sure it does not overlap */


    
    a{

        
    border: 1px rgba(05, 09, 13, 0.2) solid !important; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: rgb(205, 209, 213)!important; /* rgb(20, 26, 40, 0.91); Set a background color */
    color: black; /* Text color */
    cursor: pointer !important ; /* Add a mouse pointer on hover */
    padding: 5px; /* Some padding */
    margin-bottom:1px;
    font-size: 18px; /* Increase font size */
    transition-timing-function: ease;
    transition: background-color 2s;
    border-top-left-radius: 3999px;
    border-top-right-radius: 9999px;
    border-bottom-right-radius: 9999px;
    border-bottom-left-radius: 3999px;
    :hover {
        border: none !important;
        background-color: red !important; 
        opacity:0.4;
        
    }
    }

`

/**Div */
const StDiv2 = styled.div`

/* min-height: 20vh; */
margin: 5px 0 25px 0;
padding: 25px 10px 10px 20px;
/* font-family: Arial, Helvetica, sans-serif; */
font-family: 'Heebo', sans-serif ;
border-radius : 5px;
td{
    width: 11.11%;
}

.thc{
    /* border-bottom: 0.5px solid white;     */
    width: 11.11%;
}


.scroll {
    border-top:0.5px solid rgb(205, 209, 213); 
  height: 190px;
  overflow-x: hidden;
  overflow-y: auto;
  /* margin-top: 10px; */
}

.marginb{
   
    margin-bottom: 5px;
}
.centerme{
   
   display:flex;
   align-items:center;
   justify-content:center;
}

.carousel-indicators{
border-radius: 5px;
    border: solid whitesmoke 1px  ;
    background-color: rgba(0, 0, 0, 0.2);

       border-top-left-radius: 9999px;
    border-top-right-radius: 9999px;
    border-bottom-right-radius: 9999px;
    border-bottom-left-radius: 9999px;
li{
    height:15px;
    width: 15px;
    background-color:black;
    border-radius: 50%;
}
}

.carousel-item{
    /* background-color: rgb(222, 226, 230); */
    width: 80vw;
    margin: 0 auto 0 auto;
}
.carousel-control-next, .carousel-control-prev{
width: 5%;
}

.carousel-control-prev-icon, .carousel-control-next-icon{
    background-color: rgb(0, 0, 0, 0.95);
 }


.carousel-control-next{
    right: -10px;
    }

.btn{
    margin: 5px 5px 5px 5px;
    min-width: 100px;

    border-top-left-radius: 9999px;
    border-top-right-radius: 9999px;
    border-bottom-right-radius: 9999px;
    border-bottom-left-radius: 9999px;
}
    

    .carousel-inner{
        width: 90%;
        display:flex;
        margin: 0 auto 0 auto;
    }

.carousel{
    height: 45vh;  
    margin: 15px 0 25px ;
    margin: 0 auto 0 auto;
    
}

/* h4{
    margin: 0 0 0 100px ;
} */

h5{
    margin: 10px 0 10px 0 ;
}

.montttable{
    /* border: solid rgba(10,10,10, 0.2) 1px; */
    /* box-shadow: 10px black; */
    overflow-x:scroll;
}
`