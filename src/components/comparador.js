import React, { useEffect, useState } from 'react'
import Layout from './layout/layout'
import ToTopButton from './topButton'
import { Col, Navbar, Row, Container, Table, Button, Form, OverlayTrigger, Tooltip, Carousel } from 'react-bootstrap'
import styled from 'styled-components'
import { getRealTimeRadar, } from '../services/trader'
import {localStoragem, adicionaLST} from '../util/localsthandler'

const Comparador = () => {

    const [arrdecomp, setarrdecomp] = useState([])
    const [fullData, setFullData] = useState([])

    const [aaObjeto, setAaObjeto] = useState({})
    const [bbObjeto, setBbObjeto] = useState({})


    const [aatString, setAatString] = useState("")
    const [bbtString, setBbtString] = useState("")


    const menorVence = [
        "DivBrutPatrim", "rsiToNode", "dividaLiqToNode", "precoPorLucro", "mm9distAbs",
        "rsi_storch_fast", "rsiMedia", "rsimdeltapreco", "ascMM9", "pvpnow"
    ]

    const maiorVence = [
        "pontosTecnicos", "vlrIntrinsecoPorCotacao", "dyNow", "pontosFundamentais", "liqCorr", "margemEbit",
        "margemLiquida", "roe", "qualPrecoFund", "qualSaudeFund", "pontosSoma", "lpa","lpaMenosPvp"
    ]

    const carregarUm = () => {
       
        const getdataOne = getRealTimeRadar()
            .then((res) => {
                setFullData(res.data)
            })
    }

    useEffect(() => {
        // return () => {
        carregarUm()
        // };
    }, [])


    const filtrarDupla = () => {

        let a = fullData.filter(item => item.TICKER == aatString)[0]
        let b = fullData.filter(item => item.TICKER == bbtString)[0]
        adicionaLST(aatString, bbtString)
        setAaObjeto(a)
        setBbObjeto(b)

    }


    const quemVence = (valuea, valueb, me, par) => {
        let ref;

        if (valuea == valueb) {
            return false
        } else if (menorVence.includes(par)) {
            ref = Math.min(valuea, valueb)
        } else if (maiorVence.includes(par)) {
            ref = Math.max(valuea, valueb)
        }

        return me == ref
    }

    const makearr = (ara, arb) => {
        let arrc = []
        
        let u = []
        for (const [key, value] of Object.entries(ara)) {
            arrc.push([key, value, arb[key]])
            // u.push(key)
        }
        // console.log(JSON.stringify(u))
        setarrdecomp(arrc)
    }

    useEffect(() => {
        makearr(aaObjeto, bbObjeto)
    }, [aaObjeto, bbObjeto])


    const handleChange = (attr) => {
        const { value, name } = attr.target

        name === "aticker" ? setAatString(value.toUpperCase()) : setBbtString(value.toUpperCase())

    }

    // const adicionaLST = (a, b)=>{

    //     localStoragem("SAVE", "VISITADAS", [a, b] )
    // }
    

    
    useEffect(() => {

        let atual = []
        const vis = JSON.parse( localStoragem('GET', 'VISITADAS'))

if (vis){
        atual = [...vis,""]
}else{
atual = ["", ""]
}
        setAatString(atual[0] )
        setBbtString(atual[1] )
    }, [])

    return (
        <Container>
            <Layout header={`Comparador`}>

                <h4> Comparador </h4>


                <Encapsulador>                  
                   

                    <STTable striped bordered hover >
                        <thead>
                            <th>
                            
                            {/* <Button onClick={() => adicionaLST(aatString , bbObjeto)} variant='secondary' > adicionaLST </Button> */}
                                <Button onClick={() => filtrarDupla()} > Makearr </Button>
                            </th>
                            <th>
                                <Form.Control className="heebof" type="text" onChange={handleChange} name="aticker" value={aatString.toUpperCase() || ""} />
                            </th>
                            <th>
                                <Form.Control className="heebof" type="text" onChange={handleChange} name="bticker" value={bbtString.toUpperCase() || ""} />
                            </th>
                        </thead>

                        <tbody className="no-scroll">
                            {arrdecomp.map(item =>

                                <Trow>
                                    <td className="thc"> {item[0]} </td>
                                    <td className={quemVence(item[1], item[2], item[1], item[0]) ? "verde" : ""}> {item[1]} </td>
                                    <td className={quemVence(item[1], item[2], item[2], item[0]) ? "verde" : ""}> {item[2]} </td>
                                </Trow>
                            )}
                        </tbody>

                    </STTable>


                </Encapsulador>

                <ToTopButton />
            </Layout>
        </Container>
    )

}
export default Comparador

const Encapsulador = styled.div`
height:75vh;
.xbtn{
    padding: 3px 1px 1px 2px;
    margin: 5px 5px 5px 5px;
    width: 100px;
    height: 40px;
    border: solid 1px black;
    border-top-left-radius: 9999px;
    border-top-right-radius: 9999px;
    border-bottom-right-radius: 9999px;
    border-bottom-left-radius: 9999px;

.innerBtn{
    /* margin: 5px 5px 5px 5px; */
    width: 33%;
    height: 90%;
    border: solid 1px black;
    border-radius: 50%;
}
}
`


const Trow = styled.tr`
th, td{
    text-align:center;
}
.verde{
    background-color: #28a745; 
}
`

export const STTable = styled(Table)`
overflow: auto;
table {
  /* width: 100%; */
  
}

th{
    text-align:center;
    text-overflow: ellipsis;
    overflow: hidden;
};



thead, tbody tr {
  display: table;
  width: 100%;
  table-layout: fixed;
}

tbody {
  display: block;
  overflow-y: auto;
  table-layout: fixed;
  /* max-height: 700px; */
  height:65vh;
}

.no-scroll {
  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none; /* Firefox */
}
.no-scroll::-webkit-scrollbar {
  display: none; /* Chrome, Safari and Opera */
}


    
`