
import React from 'react'
import { Button, Table, Container, Row, ProgressBar, Col } from 'react-bootstrap'
import styled from 'styled-components'
import arred from '../util/arred'
import formatarNumero from '../util/formatingnumbers'

const UncoverTable = (props) => {

    const finanSoma = props.data.callsItmUncoveredQtd + props.data.putsItmUncoveredQtd
    const ratioQtdCall = props.data.callsItmUncoveredQtd / finanSoma
    const ratioQtdPut = 1 - ratioQtdCall
    const maxPrice = Math.max(props.data.realTimeAssetPrice, props.data.dayBfreAssetPrice,
        props.data.callUncvrdAvgStrike, props.data.putsUncvrdAvgStrike)

    return (
        <Container>

            <Row >
                <Div>
                    <h1> {props.data.ticker}   <span className="spanh1"> {props.data.dataPesquisa} </span> </h1>

                    <h4>Realtime*:  {formatarNumero(props.data.realTimeAssetPrice, "R$", 2)}
                        === Dia anterior: {formatarNumero(props.data.dayBfreAssetPrice, "R$", 2)}. Séries: {props.data.series}
                    </h4>

                    <Table >
                        <thead>
                            <tr>
                                <th> </th>
                                <th>
                                    AvgAward
                                </th>
                                <th>
                                    UncoveredQtd
                                </th>
                                <th>
                                    finanUcvrd
                                </th>
                                <th>
                                    finanRatio
                                </th>
                                <th>
                                    UncvrdAvgStrike
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr className="call-class">
                                <td  >Call</td>
                                <td>{formatarNumero(props.data.callAvgAward, "R$", 2)}</td>
                                <td>{formatarNumero(props.data.callsItmUncoveredQtd, "", 0)}</td>
                                <td>{formatarNumero(props.data.finanCallUcvrdItm, "R$", 2)}</td>
                                <td>{props.data.finanUcvrdCallItmRatio}</td>
                                <td>{formatarNumero(props.data.callUncvrdAvgStrike, "R$", 2)}</td>
                            </tr>

                            <tr>
                                <td>Put</td>
                                <td>{formatarNumero(props.data.putsAvgAward, "R$", 2)}</td>
                                <td>{formatarNumero(props.data.putsItmUncoveredQtd, "", 0)}</td>
                                <td>{formatarNumero(props.data.finanPutUcvrdItm, "R$", 2)}</td>
                                <td>{props.data.finanUcvrdPutsItmRatio}</td>
                                <td>{formatarNumero(props.data.putsUncvrdAvgStrike, "R$", 2)}</td>
                            </tr>
                        </tbody>

                    </Table>

                    <hr />
                    <Row>
                        <Col xs={3} >
                            <p> Proporção Uncover Financeiro</p>

                        </Col>

                        <Col>
                            <div className="callputratio">
                                <Bar w={props.data.finanUcvrdCallItmRatio * 100} cor={'green'} >{arred(props.data.finanUcvrdCallItmRatio * 100)} </Bar>
                                <Bar w={(1 - props.data.finanUcvrdCallItmRatio) * 100} cor={'red'}>{arred((1 - props.data.finanUcvrdCallItmRatio) * 100)}</Bar>
                            </div>
                        </Col>
                    </Row>

                    <br />
                    <Row>
                        <Col xs={3} >
                            <p> Proporção Uncover Quantidade</p>
                        </Col>

                        <Col>
                            <div className="callputratio">
                                <Bar w={ratioQtdCall * 100} cor={'green'} >{arred(ratioQtdCall * 100)} </Bar>
                                <Bar w={ratioQtdPut * 100} cor={'red'}>{arred(ratioQtdPut * 100)}</Bar>
                            </div>
                        </Col>
                    </Row>

                    <br />

                    <Row>
                        <Col xs={3}>
                            {props.data.callUncvrdAvgStrike < props.data.realTimeAssetPrice ? <Button variant="success">Call Warn </Button> : ""}
                            {props.data.putsUncvrdAvgStrike > props.data.realTimeAssetPrice ? <Button variant="danger">Puts Warn</Button> : ""}
                        </Col>
                        <Col>

                            <div>
                                <p> Asset Price vs Strike Compare:</p>
                            </div>

                            <div>
                                <STProgressBar label={`Call AVG Strike ${props.data.callUncvrdAvgStrike}`} variant="success" now={100 * props.data.callUncvrdAvgStrike / maxPrice} />
                                <STProgressBar label={`Put AVG Strike  ${props.data.putsUncvrdAvgStrike}`} variant="danger" now={100 * props.data.putsUncvrdAvgStrike / maxPrice} />
                                <STProgressBar label={`Realtime Price  ${props.data.realTimeAssetPrice}`} variant="warning" now={100 * props.data.realTimeAssetPrice / maxPrice} />
                                <STProgressBar label={`D-1 price  ${props.data.dayBfreAssetPrice}`} variant="info" now={100 * props.data.dayBfreAssetPrice / maxPrice} />
                            </div>
                        </Col>
                    </Row>

                    <hr />
                    <Row>
                        <Col xs={3}>
                            <p> Top Uncover Call:</p>
                        </Col>
                        <Col >
                            {props.data.topCallUnc ?
                                < div className={'callputratio'}>
                                    {props.data.topCallUnc.map(item =>
                                        // <Button variant="success">{item.optTicker}</Button>
                                        <div className={'w-vintep'}> {item.optTicker} &nbsp; {item.strike}</div>
                                    )}
                                </div>
                                : ""}
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={3}>
                            <p> Top Uncover Puts:</p>
                        </Col>
                        <Col >
                            {props.data.topPutsUnc ?
                                < div className={'callputratio'}>
                                    {props.data.topPutsUnc.map(item =>
                                        // <Button variant="success">{item.optTicker}</Button>
                                        <div className={'w-vintep'}> {item.optTicker} &nbsp; {item.strike}</div>
                                    )}
                                </div>
                                : ""}
                        </Col>
                    </Row>

                </Div>
            </Row>
        </Container>
    )
}

export default UncoverTable

const STProgressBar = styled(ProgressBar)`
margin-bottom: 5px;
height: 20px;
`

const Div = styled.div`
border: 1px solid black;
padding: 10px;
margin-bottom: 10px;
width: 100%;
border-radius: 5px;

.spanh1{
color:red;
font-size:20px;
}

.callputratio{
    display: flex;
}

.call-class{
    color: green;
}
.w-vintep {
    width: 20%;
    border: 1px solid black;
    border-radius: 5px;
    text-align: center;
    margin: 5px;
}

`

const Bar = styled.div`   
    width: ${props => `${props.w}%` || `auto`};
    height: 20px;
    background-color: ${props => props.cor};
    color: white;
    text-align: center;

`
