import React from 'react'
import Destaques from './destaques'
import Layout from './layout/layout'
import ToTopButton from './topButton'


const Tabelasdestaques = () => {
  return (
    <>
    <Layout header={`Trading Destaques fixados`}>
    <Destaques tabela/>
      <ToTopButton/>
      </Layout>
    </>
  )
}

export default Tabelasdestaques
