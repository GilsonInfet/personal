import React, { useEffect, useState } from 'react'
import { Col, Navbar, Row, Container, Table, Button, DropdownButton, Dropdown, ButtonGroup } from 'react-bootstrap'
import styled from 'styled-components'
import { getRealTimeRadar, todasOpcoesCalculadas } from '../services/trader'
import optTipo from '../util/functioncallput'
import { acao } from '../util/objeto'
import history from '../config/history';
import Destaques from './destaques'
import ToTopButton from './topButton'
import Layout from './layout/layout'
import Listr from './destaquesv.js'
import { adicionaLST} from '../util/localsthandler'

export function isBuy(item) {
    let temp;

    if (optTipo(item.optTicker) === "CALL") {
        temp = item.coacaoAtivo > item.strike
    } else {
        temp = item.coacaoAtivo < item.strike
    }
    return temp

}


const AllOptionsListed = () => {
    /**Força o update no usestate */
    const [forceUpdate, setForceUpdate] = useState(1)
    const [fullData, setFullData] = useState(acao.opcoesAtmListCalculadas)
    /**As opções calsuladas e que serão exibidas são estas: */
    const [filtrada, setFiltrada] = useState(fullData)
    /**Series existentes */
    const [seriesExistentes, setSeriesExistentes] = useState([])

    const [autoRefresh, setautoRefresh] = useState(false)

    const originalFilter = {
        itm: false,
        seriesVisualizadas: [],
        tipo: ""
    }
    const [multiFilter, setMultiFilter] = useState(originalFilter)


    const defineSeries = (e) => {
        let temp = multiFilter.seriesVisualizadas
        if (temp.includes(e)) {
            const index = temp.indexOf(e);
            temp.splice(index, 1);
        } else {
            temp.push(e)
        }

        setMultiFilter({
            ...multiFilter,
            seriesVisualizadas: temp
        })
    }

    /**Deve adicionar ou inserir as séries a serem exibidas na lista */
    const aplicarFiltros = () => {

        let ser = [...fullData]

        ser = ser.filter(item => multiFilter.seriesVisualizadas.includes(item.serie))

        if (multiFilter.tipo) {
            ser = ser.filter(item => optTipo(item.optTicker) == multiFilter.tipo)
        }

        if (multiFilter.itm) {
            ser = ser.filter(item => isBuy(item))
        }



        setFiltrada(ser)

    }

    function ajustaFloat(item) {
        let x = ""

        try {
            x = item.replace(",", ".")
        } catch {
            x = 0
        }

        return parseFloat(x) || 0
    }



    const removerTodosOsFiltros = () => {

        setMultiFilter(
            {
                ...originalFilter,
                seriesVisualizadas: [...seriesExistentes]
            })
    }


    const carregarTodasOpcoesCalculadas = async () => {

        await todasOpcoesCalculadas()
            .then((res) => {
            
                res.data.map(item => {

                    if (!seriesExistentes.includes(item.serie)) {
                        seriesExistentes.push(item.serie)
                        multiFilter.seriesVisualizadas.push(item.serie)
                    }

                })

                setSeriesExistentes(seriesExistentes.sort(function (a, b) {
                    if (a > b) { return 1 }
                    if (a < b) { return -1 }
                    return 0;
                }))

                res.data = res.data.filter(item => item.optTicker)

                res.data = res.data.sort(function (a, b) {
                    if (a.premio > b.premio) {
                        return 1;
                    }
                    if (a.premio < b.premio) {
                        return -1;
                    }
                    return 0;
                })

                setFullData(res.data)
         
                if (autoRefresh || filtrada.length <= 1) {
                    setFiltrada(res.data)
                }
            })
    }



    const sortFiltrada = (i) => {
        let temp = sorteada(i)
        setFiltrada(temp)
        setForceUpdate(1 + forceUpdate)
    }




    const sorteada = (sortearPor = "TICKER") => {

        return filtrada.sort(function (a, b) {
            if (a[sortearPor] > b[sortearPor]) { return 1 }
            if (a[sortearPor] < b[sortearPor]) { return -1 }
            return 0;
        })
    }


    const sortFiltradaAllDataObj = (i) => {
        let temp = sorteadaAllDataObj(i)
        setFiltrada(temp)
        setForceUpdate(1 + forceUpdate)
    }

    const sorteadaAllDataObj = (sortearPor = "TICKER") => {

        return filtrada.sort(function (a, b) {
            if (a.allDataObj[sortearPor] > b.allDataObj[sortearPor]) { return 1 }
            if (a.allDataObj[sortearPor] < b.allDataObj[sortearPor]) { return -1 }
            return 0;
        })
    }

    useEffect(
        () => {

            aplicarFiltros()
        }, [multiFilter])

    useEffect(
        () => {


        }, [filtrada, forceUpdate])

    useEffect(
        () => {
            carregarTodasOpcoesCalculadas()
        }, [])


    useEffect(() => {
        const interval = setInterval(() => {
            carregarTodasOpcoesCalculadas()
        }, 60 * 1000);
        return () => clearInterval(interval);
    }, []);

    const Linkes = (props) => {
        return (
            <Dropdown as={ButtonGroup}>
                <Dropdown.Toggle variant="secondary" id="dropdown-split-basic" />

                <Dropdown.Menu  onClick={()=>adicionaLST(props.ticker.TICKER)}>
                    <Dropdown.Item
                        target="_blank" rel="noopener noreferrer"
                        href={`https://www.fundamentus.com.br/detalhes.php?papel=${props.ticker.TICKER}`}>Fundamentus</Dropdown.Item>

                    <Dropdown.Item
                        target="_blank" rel="noopener noreferrer"
                        href={`https://www.tradingview.com/symbols/BMFBOVESPA-${props.ticker.TICKER}/technicals/`}>Trading View</Dropdown.Item>

                    <Dropdown.Item
                        target="_blank" rel="noopener noreferrer"
                        href={`https://opcoes.net.br/calculadora-Black-Scholes/${props.ticker.optTicker}`}>Black Scholes</Dropdown.Item>

                    <Dropdown.Item
                        target="_blank" rel="noopener noreferrer"
                        href={`https://br.tradingview.com/chart/?symbol=BMFBOVESPA%3A${props.ticker.TICKER}`}>GraficoTRV</Dropdown.Item>

                    <Dropdown.Item
                        target="_blank" rel="noopener noreferrer"
                        href={`https://twitter.com/search?q=%23${props.ticker.TICKER}&src=typed_query&f=live`}>Twitter</Dropdown.Item>

                    <Dropdown.Item
                        target="_blank" rel="noopener noreferrer"
                        href={`https://opcoes.net.br/opcoes/bovespa/${props.ticker.TICKER}`}>Opções Net</Dropdown.Item>


                    <Dropdown.Item>
                        <Button onClick={() => history.push(`trade/${props.ticker.TICKER}`)}
                            variant="link"
                        >  Scan  </Button>

                    </Dropdown.Item>

                    <Dropdown.Item>
                        <Button onClick={() => history.push(`calculadora/${props.ticker.optTicker}`)}
                            variant="link"
                        >  Calculadora  </Button>

                    </Dropdown.Item>


                </Dropdown.Menu>
            </Dropdown>


        )
    }


    return (

        <Container fluid>
            <Layout header={`Todas as opções.`}>

                {/* <Destaques /> */}
                <StDiv>
                    <Row>
                        <Button

                            variant={multiFilter.tipo == "CALL" ? "secondary" : "outline-secondary"}
                            onClick={() =>

                                setMultiFilter({
                                    ...multiFilter,
                                    tipo: multiFilter.tipo == "CALL" ? "" : "CALL"
                                })
                            }>Calls</Button>

                        <Button
                            variant={multiFilter.tipo == "PUT" ? "danger" : "outline-danger"}
                            onClick={() =>
                                setMultiFilter({
                                    ...multiFilter,
                                    tipo: multiFilter.tipo == "PUT" ? "" : "PUT"
                                })
                            }>Puts</Button>

                        <Button variant={multiFilter.itm ? "success" : "outline-success"}
                            onClick={() =>
                                setMultiFilter({
                                    ...multiFilter,
                                    itm: !multiFilter.itm
                                })
                            }>Apenas ITM</Button>


                        {seriesExistentes.map(item => (
                            <Button variant={multiFilter.seriesVisualizadas.includes(item) ? "info" : "outline-info"} onClick={() => defineSeries(item)}>Série {item}</Button>
                        ))}

                        <Button variant="outline-info" onClick={removerTodosOsFiltros}>Sem Filtro</Button>

                        <Button 
                        variant={autoRefresh ? "info":  "outline-info"} 
                        onClick={() => setautoRefresh(!autoRefresh)}
                        >{autoRefresh ? "Auto Refresh ON": "Auto Refresh OFF"}</Button>

                    </Row>
                    <br /> <br />
                    <Row>
                        <br /><br /><br />
                        <h4>Opções calculadas. Exibindo {filtrada.length} itens.</h4>
                        <br />

                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                    <th>
                                        <Button variant="link" onClick={() => sortFiltrada('TICKER')}>Ticker do Ativo</Button>
                                    </th>
                                    <th >Ticker da Opção</th>
                                    <th>Série</th>
                                    <th>Tipo</th>
                                    <th>
                                        <Button variant="link" onClick={() => sortFiltrada('premio')}>Prêmio Estimado</Button>
                                    </th>
                                    <th>Strike</th>
                                    <th>Cotação</th>
                                    <th>Strike/Cotação</th>
                                    <th> <Button variant="link" onClick={() => sortFiltrada('atualPorMM9')}> Atual / MM9</Button></th>

                                    <th>
                                        <Button variant="link" onClick={() => sortFiltradaAllDataObj('deltaVol')}> Delta Vol</Button>
                                    </th>

                                    <th>DeltaPreco</th>
                                    <th>Momento do cálculo</th>
                                    <th>Links</th>
                                </tr>
                            </thead>
                            <tbody>

                                {filtrada.map(item => (
                                    <tr>
                                        <TDisbuy buy={isBuy(item)}> {item.TICKER}</TDisbuy>
                                        <td> {item.optTicker}</td>
                                        <td> {item.serie}</td>
                                        <td> {optTipo(item.optTicker)}</td>
                                        <td> {item.premio}</td>
                                        <td> {item.strike}</td>
                                        <td> {item.coacaoAtivo}</td>
                                        <td>  {(((item.strike / item.coacaoAtivo) - 1) * 100).toFixed(2)}%</td>
                                        {/* <td> {(((item.strike / item.coacaoAtivo)-1)*100).toFixed(2)}%</td> */}
                                        <td> {item.allDataObj?.atualPorMM9}</td>
                                        <td> {item.allDataObj?.deltaVol}</td>
                                        <td> {item.allDataObj?.deltaPreco}</td>
                                        <td> {item.data}</td>
                                        <td className="centerme"><Linkes ticker={item} /></td>
                                    </tr>
                                ))}

                            </tbody>
                        </Table>
                    </Row>

                </StDiv>
                <ToTopButton />
            </Layout>
        </Container>
    )
}

export default AllOptionsListed


/**Div */
const StDiv = styled.div`
background-color: white;
min-height: 100vh;
padding: 20px;
/* font-family: Arial, Helvetica, sans-serif; */
font-family: 'Heebo', sans-serif ;

.btn{
margin: 0 5px 0 5px;
min-width: 100px;
}

td a .btn{
margin-left: auto; 
margin-right: auto;
}

td a {
margin-left: auto; 
margin-right: auto;

}

.centerme{
   
   display:flex;
   align-items:center;
   justify-content:center;
}

th{
 text-align:center;
}
h4, th, td{
    font-family: 'Heebo', sans-serif ;
    font-weight: 900;
    text-align:center;
}
.center{
    background-size: 100% 100%;  //ajusta as dimensões da imagem às da DIV
    display:flex;
align-items:center;
justify-content:center;
transition : 1000ms;
width: 250px;
height: 360px;
}

`

export const TDisbuy = styled.td`
background-color: ${props => props.buy ? `#28a745` : `transparent`}; 

`


