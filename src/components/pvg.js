

import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import styled from 'styled-components'
import { getRealTimeRadar } from '../services/trader';
import history from '../config/history';

const Pvgalert = (props) => {

    const [exibe, setExibe] = useState([])

    const carregarUm = async () => {

        await getRealTimeRadar()
            .then((res) => {

               try {                   
                   const filtrado = res.data.filter(item => item.theGap > 0.005)
                   .filter(item => item.deltaVol > 0.9)
                   .filter(item => item.deltaPreco > 0.01)                   
                   setExibe(filtrado)
                } catch (error) {
                    
                }

            })

    }




    useEffect(() => {
        carregarUm()        
    }, [])

    useEffect(() => {
        if (!("Notification" in window)) {
            console.log("This browser does not support desktop notification");
          } else {
            Notification.requestPermission();
          }        
    }, [])

    function notifyMe() {
 
        // Let's check if the browser supports notifications
        if (!("Notification" in window)) {
          alert("This browser does not support desktop notification");
        }
      
        // Let's check whether notification permissions have already been granted
        else if (Notification.permission === "granted") {
          // If it's okay let's create a notification
          var notification = new Notification("Hi there!");
        }
      
        // Otherwise, we need to ask the user for permission
        else if (Notification.permission !== "denied") {
          Notification.requestPermission().then(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
              var notification = new Notification("Hi there!");
            }
          });
        }
      
        // At last, if the user has denied notifications, and you
        // want to be respectful there is no need to bother them any more.
      }

    useEffect(() => {
        const interval = setInterval(() => {
            carregarUm() 
        }, 60 * 1000);
        return () => clearInterval(interval);
      }, []);

    const verPapeis = () => {
        const exibed = exibe.map(item => { return item.TICKER })
        // history.push('/tabelasdestaques#fastOtimo')
        alert(exibed.join("; "))
    }

    return (
        <>
        { exibe.length > 0 ?
                <Divanim>
                    <Btnfixed
                        className={exibe.length > 0 ? "animated fadeIn" : "animatedOut fadeOut"}
                        exibe={true}
                        pointer={exibe}
                        onClick={exibe ? () => verPapeis() : ""}
                        // onClick={exibe ? () => notifyMe() : ""}
                        
                        id="myBtn" >

                        {exibe.length}

                    </Btnfixed>
                </Divanim>
                : ""
        }
        </>
    )

}

export default Pvgalert


const Divanim = styled.div`
         .animated {
            -webkit-animation-duration: 2s;
            animation-duration: 2s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            -webkit-animation-name: fadeIn;
            animation-name: fadeIn;
            border: none !important;
         }
         
         @-webkit-keyframes fadeIn {
            0% {opacity: 0;}
            100% {opacity: 1;}
         }
         
         @keyframes fadeIn {
            0% {opacity: 0;}
            100% {opacity: 1;}
        }

        .animatedOut {
            -webkit-animation-duration: 2s;
            animation-duration: 2s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            -webkit-animation-name: fadeOut;
            animation-name: fadeOut;
            border: none !important;            
         }
         
         @-webkit-keyframes fadeOut {

            0% {opacity: 1;}
            100% {opacity: 0;}
         }
         
         @keyframes fadeOut {
            0% {opacity: 1;}
            100% {opacity: 0;}
         }
        `

const Btnfixed = styled.button`
width: 50px;
height: 50px;

display:${props => props.exibe ? `block` : `none`};
    position: fixed; /* Fixed/sticky position */
    top: 9px; /* Place the button at the bottom of the page */
    left: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none !important; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: rgb(200, 10, 10, 0.91)!important; /* rgb(20, 26, 40, 0.91); Set a background color */
    color: black; /* Text color */
    cursor: ${props => props.pointer ? `pointer` : `auto`} !important ; /* Add a mouse pointer on hover */
    padding: 15px; /* Some padding */
    border-radius: 50%; /* Rounded corners */
    font-size: 18px; /* Increase font size */
    transition-timing-function: ease;
    transition: background-color 2s;
    animation: animation2 5s infinite;
    color: white;
    :hover {
        border: none !important;
        background-color: #fac564 !important; 
        opacity:0.4
    }

    .circle1{
    top:-8%;
    right: -20%;
    background: linear-gradient(rgb(0,255,179),  rgb(0,255, 79));
    animation: animation2 5s infinite;
}

    @keyframes animation2 {
  0%   {background: linear-gradient(rgb(0,255,179),  rgb(0,255, 79))}
  50%  {background: linear-gradient(rgb(0,255,179),  rgb(0,255, 79))}
  100% {background: linear-gradient(rgb(0,255,179),  rgb(0,255, 79))}
}


`
