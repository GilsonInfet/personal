import React, { useEffect, useState } from 'react'
import { Container, Button } from 'react-bootstrap'
import styled from 'styled-components'
import { getTwitts } from '../services/trader'
import Layout from '../components/layout/layout'
import { useParams } from 'react-router-dom'

/**Lê radarRealtime: `cache/radarRealtime.csv` */
const TwittsView = (props) => {

    const [twitts, setTwitts] = useState([{}])
    const [order, setOrder] = useState('tamanho')
    const { paper } = useParams()



    const carregarUm = async () => {
        const query = {
            ticker: paper,
            startDate: '2021-09-20'
        }
        await getTwitts(query)
            .then((res) => {

                setTwitts(res.data.sort(
                    function (a, b) {
                        if (a[order] > b[order]) {
                            return -1;
                        }
                        if (a[order] < b[order]) {
                            return 1;
                        }
                        return 0;
                    }

                ))
            })

    }


    const formatVal = (val) => {

        if (!val) return '-'
        return val.toLocaleString('pt-br', { minimumFractionDigits: 0, maximumFractionDigits: 2 });
    }

    useEffect(
        () => {
            carregarUm()
        }, [order])


    return (

        <Layout header={`Aluguéis.`}>
            <StDiv2>
                <Container>
                    <h1>Número de twittes:  {twitts.length}</h1>

                    <br />

                    {twitts.map(item => (
                        <div className="item-aluguel">
                            <p> {item?.ticker} {item?.tamanho}  </p>
                            {item.data?.map(da => (
                                <div>
                                    {/* <p> {da.id_str}  </p> */}
                                    <div> <a href={`https://twitter.com/anyuser/status/${da.id_str}`}   target="_blank" rel="noreferrer"> Link</a> == {da.texto}  </div>
                                </div>
                            ))}
                            <br />
                        </div>

                    ))}


                </Container>

            </StDiv2>
        </Layout>
    )
}

export default TwittsView



/**Div */
const StDiv2 = styled.div`

margin: 5px 0 25px 0;
padding: 25px 10px 10px 20px;
font-family: 'Heebo', sans-serif ;
border-radius : 5px;

    tr{
    th{
        text-align: center;
    }
}

.item-aluguel{
    border: 1px solid black;
    border-radius : 5px;
    padding 5px;
    margin-bottom: 10px;
}

.btn{
    margin: 5px 5px 5px 5px;
    min-width: 100px;
    border-top-left-radius: 9999px;
    border-top-right-radius: 9999px;
    border-bottom-right-radius: 9999px;
    border-bottom-left-radius: 9999px;
}  


h5{
    margin: 10px 0 10px 0 ;
}


`