import React, { useEffect, useState } from 'react'
import { Container, Table, ProgressBar, Button } from 'react-bootstrap'
import styled from 'styled-components'
import { aluguel } from '../services/trader'

import Layout from '../components/layout/layout'
import arred from '../util/arred'

/**Lê radarRealtime: `cache/radarRealtime.csv` */
const AluguelView = (props) => {

    const [uncoveredBal, setUncoveredBal] = useState([{}])
    const [order, setOrder] = useState('diffTotal')




    const carregarUm = async () => {

        await aluguel()
        .then((res) => {
                debugger
                setUncoveredBal(res.data.sort(
                    function (a, b) {
                        if (a[order] > b[order]) {
                            return -1;
                        }
                        if (a[order] < b[order]) {
                            return 1;
                        }
                        return 0;
                    }

                ))
            })

    }


    const formatVal = (val) => {

        if (!val) return '-'
        return val.toLocaleString('pt-br', { minimumFractionDigits: 0, maximumFractionDigits: 2 });
    }

    useEffect(
        () => {
            carregarUm()
        }, [order])


    return (

        <Layout header={`Aluguéis.`}>
            <StDiv2>
                <Container>
                    <Button onClick={() => setOrder('diffTotal')}> Ordenar Dif Total</Button>
                    <Button onClick={() => setOrder('diffQotationAvgPrice')}> Ordenar Dif Preço</Button>
                    <Button onClick={() => setOrder('aluguelFreeFloatRatio')}> Ordenar % Free Float</Button>
                    <Button onClick={() => setOrder('atention')}> Preço atípico </Button>
                    
                    <br />

                    {uncoveredBal.map(item => (
                        <div className="item-aluguel">
                            <p> {item.ticker}  </p>

                            {item.TradAvrgPric < item.qotacao ?
                                <Button > Atenção</Button> : ""
                            }
                            <Table striped bordered hover variant="dark">
                                <thead>
                                    <tr>
                                        <th>Cotação Now*</th>
                                        <th>PM Aluguel</th>
                                        <th>Dif. Preço</th>
                                        <th>Dif. % Preço</th>
                                        
                                        <th>Total Ações</th>
                                        <th>Free Float</th>
                                        <th>Alugadas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> {formatVal(item.qotacao)} </td>
                                        <td> {formatVal(item.TradAvrgPric)} </td>
                                        <td> {formatVal(item.diffQotationAvgPrice)} </td>
                                        <td> {formatVal(item.diffPriceRatio * 100)}% </td>
                                        <td> {formatVal(item.qtdAcoes)} </td>
                                        <td> {formatVal(item.qtdAcoesFreeFloat)} </td>
                                        <td> {formatVal(item.acoesAlugadasAgora)} </td>
                                    </tr>

                                </tbody>
                            </Table>

                            <p>Alugadas em relação ao Free Float</p>

                            <ProgressBar>
                                <ProgressBar variant="success" label={`${arred(item.aluguelFreeFloatRatio * 100)}%`} now={item.aluguelFreeFloatRatio * 100} key={1} />
                                <ProgressBar variant="danger" label={`${arred(100 - item.aluguelFreeFloatRatio * 100)}%`} now={100 - item.aluguelFreeFloatRatio * 100} key={3} />
                            </ProgressBar>

                            <br />
                        </div>

                    ))}


                </Container>

            </StDiv2>
        </Layout>
    )
}

export default AluguelView



/**Div */
const StDiv2 = styled.div`

margin: 5px 0 25px 0;
padding: 25px 10px 10px 20px;
font-family: 'Heebo', sans-serif ;
border-radius : 5px;

    tr{
    th{
        text-align: center;
    }
}

.item-aluguel{
    border: 1px solid black;
    border-radius : 5px;
    padding 5px;
    margin-bottom: 10px;
}

.btn{
    margin: 5px 5px 5px 5px;
    min-width: 100px;
    border-top-left-radius: 9999px;
    border-top-right-radius: 9999px;
    border-bottom-right-radius: 9999px;
    border-bottom-left-radius: 9999px;
}  


h5{
    margin: 10px 0 10px 0 ;
}


`