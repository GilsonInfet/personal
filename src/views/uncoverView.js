import React, { useEffect, useState } from 'react'
import { Container, } from 'react-bootstrap'
import styled from 'styled-components'
import { uncoveredbalance } from '../services/trader'
import UncoverTable from '../components/uncoverTable'
import Layout from '../components/layout/layout'

/**Lê radarRealtime: `cache/radarRealtime.csv` */
const UncoverView = (props) => {

    const [uncoveredBal, setUncoveredBal] = useState([{}])
 
    const carregarUm = async () => {

        await uncoveredbalance()
            .then((res) => {
                setUncoveredBal(res.data.analisys)
            })

    }


    useEffect(
        () => {
            carregarUm()
        }, [])


    useEffect(() => {
        setInterval(() => {
            //ponha aqui a função que irá atualizar seu state
            carregarUm()
        }, 5 * 60 * 1000)
    }, []);


    return (

        <Layout header={`Opções Descobertas.`}>
        <StDiv2>
            <Container>
                {/* <h4>Hora Report: {uncoveredBal[0]?.dataPesquisa} </h4> */}

                {uncoveredBal.map(item => (
                    <UncoverTable data={item} />
                ))}


            </Container>

        </StDiv2>
        </Layout>
    )
}

export default UncoverView



/**Div */
const StDiv2 = styled.div`

margin: 5px 0 25px 0;
padding: 25px 10px 10px 20px;
font-family: 'Heebo', sans-serif ;
border-radius : 5px;

.thc{
    width: 11.11%;
}

.scroll {
    border-top:0.5px solid rgb(205, 209, 213); 
  height: 190px;
  overflow-x: hidden;
  overflow-y: auto;
  /* margin-top: 10px; */
}

.marginb{   
    margin-bottom: 5px;
}
.centerme{   
   display:flex;
   align-items:center;
   justify-content:center;
}

.btn{
    margin: 5px 5px 5px 5px;
    min-width: 100px;
    border-top-left-radius: 9999px;
    border-top-right-radius: 9999px;
    border-bottom-right-radius: 9999px;
    border-bottom-left-radius: 9999px;
}  


h5{
    margin: 10px 0 10px 0 ;
}

.montttable{
    /* border: solid rgba(10,10,10, 0.2) 1px; */
    /* box-shadow: 10px black; */
    overflow-x:scroll;
}
`