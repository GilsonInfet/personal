
import './App.css';
import { Route, Router, Switch } from 'react-router-dom'
import Home from './components/home';
import Dog from './components/dog';
import Smooth from './components/smooth';
import Glass from './components/glass';
import Movingbg from './components/movingbg';
import history from './config/history';
import Excel from './components/excelrender';
import Navbars from './components/navbars';
import MenuAnimado from './components/menuAnimado';
import Tenis from './components/tenis';
import Trade from './components/trade';
import AllOptionsListed from './components/allOpcoes';
import Destaquesv from './components/destaquesv';
import Tabelasdestaques from './components/tabelasdestaques';
import Comparador from './components/comparador';
import Calculadora from './components/calculadora';
import Quadro from './components/quadro';
import Paralax from './components/paralax';
import UncoverView from './views/uncoverView'
import AluguelView from './views/aluguelView'
import TwittsView from './views/twittsView'

function App() {

 
    return (
        <Router history={history}>
            <Switch>
                <Route exact component={Home}   path="/" />
                <Route exact component={Dog}    path="/dog" />
                <Route exact component={Smooth} path="/smooth" />
                <Route exact component={Glass}  path="/glass" />
                <Route exact component={Movingbg} path="/movingbg" />
                <Route exact component={Excel}  path="/excel" />
                <Route exact component={Navbars} path="/navbars" />
                <Route exact component={MenuAnimado} path="/menuanimado" />
                <Route exact component={Tenis} path="/tenis" />
                <Route exact component={Trade} path="/trade/:paper" />
                <Route exact component={Trade} path="/trade" />
                <Route exact component={AllOptionsListed} path="/alloptions" />
                <Route exact component={Destaquesv} path="/grafico" />
                <Route exact component={UncoverView} path="/uncover" />
                <Route exact component={AluguelView} path="/aluguel" />
                <Route exact component={TwittsView} path="/twitts" />
                <Route exact component={TwittsView}path="/twitts/:paper" />
                <Route exact component={Tabelasdestaques}path="/tabelasdestaques" />
                <Route exact component={Comparador}path="/comparador" />
                <Route exact component={Calculadora}path="/calculadora" />
                <Route exact component={Calculadora}path="/calculadora/:paper" />
                <Route exact component={Quadro}path="/quadro" />
                <Route exact component={Paralax}path="/paralax" />
            </Switch>
        </Router>

    );
}

export default App;
