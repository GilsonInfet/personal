import http from "../config/http";

/** vai na rota fulldata e retorna todas as informações disponíveis para um ativo dado */
const getFullData = async (query) => await http.get(`/fulldata`, {params: query});
/** Faz get em  http.get(`/opcoesatm`) que por sua vez devolve relatorioatmCalculado: `cache/relatorio.csv`, */
const todasOpcoesCalculadas = async () => await http.get(`/opcoesatm`);

const suspiciousVolumes = async () => await http.get(`/suspiciousvolumes`);

const bollinger = async () => await http.get(`/bollinger`);

const aluguel = async () => await http.get(`/aluguel`);

const uncoveredbalance = async () => await http.get(`/uncoveredbalance`);
/** passar { ticker, startDate} na query */
const getTwitts  = async (query) => await http.get(`/twitt`, {params: query});

/** Chama a rota /realtime que por sua vez Pega todos os ativos do csv no radar */
const getRealTimeRadar  = async (query) => await http.get(`/realtime`, {params: query});

/**faz get na rota /opcoestodas , passar necessariamente a query {acao: 'tickerdesejado'}*/
const getOpcoesDe = async (query) => await http.get(`/opcoestodas`, {params: query});

/** Retorna o cálculo de uma opção para um valor de cotação dado. Query deve conter { cotacao, ticker, ativoTicker, strike } */
const getCalocOneOpcao = async (query) => await http.get(`/calc`, {params: query});

export {getFullData, getTwitts, todasOpcoesCalculadas, getRealTimeRadar, getOpcoesDe, getCalocOneOpcao, suspiciousVolumes, uncoveredbalance, bollinger, aluguel}